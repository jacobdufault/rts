﻿using System;
using System.Collections.Generic;
using System.Linq;
using Forge2;
using NUnit.Framework;

namespace Game {
    public class QuadTreeTests {
        private List<T> RunSlowQuery<T>(QuadTree<T> tree, Rect queryArea) where T : IEquatable<T>, new() {
            var result = new List<T>();

            foreach (QuadTree<T>.Node[] nodes in tree.levelToNodes) {
                foreach (QuadTree<T>.Node node in nodes) {
                    foreach (QuadTree<T>.Item item in node.items) {
                        if (queryArea.Contains(item.rect) || queryArea.Intersects(item.rect))
                            result.Add(item.data);
                    }
                }
            }

            return result;
        }

        private void TestQuery<T>(QuadTree<T> tree, Rect queryRect) where T : IEquatable<T>, new() {
            var expected = RunSlowQuery(tree, queryRect);
            var actual = tree.Query(queryRect);

            Console.WriteLine("QUERY:");
            Console.WriteLine(queryRect);
            Console.WriteLine();
            Console.WriteLine("EXPECTED:");
            foreach (T item in expected)
                Console.WriteLine(item);
            Console.WriteLine();
            Console.WriteLine("ACTUAL:");
            foreach (T item in actual)
                Console.WriteLine(item);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("====");
            Console.WriteLine();
            Console.WriteLine();

            CollectionAssert.AreEquivalent(expected, actual, $"queryRect: {queryRect}");
        }

        [Test]
        public void TestQuery() {
            var tree = new QuadTree<int>(4, new Dimension(10, 10));
            TestQuery(tree, Rect.FromXYWH(0, 0, 1, 1));
            //TestQuery(tree, Rect.FromXYWH(0, 30, 5, 5));
            //TestQuery(tree, Rect.FromXYWH(0, 30, 15, 5));
            //TestQuery(tree, tree.bounds);
        }

        [Test]
        public void TestSimpleSizes() {
            var b = new Dimension(10, 15);
            Assert.AreEqual(Rect.FromWH(1 * b.w - 1, 1 * b.h - 1), new QuadTree<int>(1, b).bounds);
            Assert.AreEqual(Rect.FromWH(2 * b.w - 1, 2 * b.h - 1), new QuadTree<int>(2, b).bounds);
            Assert.AreEqual(Rect.FromWH(4 * b.w - 1, 4 * b.h - 1), new QuadTree<int>(3, b).bounds);
            Assert.AreEqual(Rect.FromWH(8 * b.w - 1, 8 * b.h - 1), new QuadTree<int>(4, b).bounds);
            Assert.AreEqual(Rect.FromWH(16 * b.w - 1, 16 * b.h - 1), new QuadTree<int>(5, b).bounds);
            Assert.AreEqual(Rect.FromWH(32 * b.w - 1, 32 * b.h - 1), new QuadTree<int>(6, b).bounds);
        }

        private static void Check<T>(QuadTree<T> tree, Rect point) where T : IEquatable<T>, new() {
            ref QuadTree<T>.Node node = ref tree.GetNodeContaining(point);
            Assert.IsTrue(node.area.Contains(point));
        }

        [Test]
        public void TestInstantation() {
            int maxNodeSize = 4;
            int maxLevels = 5;

            for (int smallestNodeX = 1; smallestNodeX < maxNodeSize; ++smallestNodeX) {
                for (int smallestNodeY = 1; smallestNodeY < maxNodeSize; ++smallestNodeY) {
                    for (int numLevels = 1; numLevels < maxLevels; ++numLevels) {
                        var tree = new QuadTree<int>(numLevels, new Dimension(smallestNodeX, smallestNodeY));

                        int expectedTotalWidth = smallestNodeX * (int)Math.Pow(2, numLevels - 1) - 1;
                        int expectedTotalHeight = smallestNodeY * (int)Math.Pow(2, numLevels - 1) - 1;
                        Assert.AreEqual(Rect.FromWH(expectedTotalWidth, expectedTotalHeight), tree.bounds);

                        for (int x = 0; x < tree.bounds.width; ++x) {
                            for (int y = 0; y < tree.bounds.height; ++y) {
                                for (int w = 0; w < tree.maxStoredSize.w; ++w) {
                                    for (int h = 0; h < tree.maxStoredSize.h; ++h) {
                                        var rect = Rect.FromXYWH(x, y, w, h);
                                        if (tree.bounds.Contains(rect))
                                            Check(tree, rect);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test]
        public void SimpleTest() {
            var tree = QuadTree<int>.CreateForDimensions(new Dimension(5000, 5000), new Dimension(10, 10));

            tree.Add(Rect.FromXYWH(50, 50, 5, 5), 1);
            tree.Add(Rect.FromXYWH(60, 50, 5, 5), 2);

            // TODO: Just because a node is returned by Query does not mean the items inside are there.
            var result = new Bag<int>();
            foreach (QuadTree<int>.Node node in tree.QueryNodes(Rect.FromXYWH(40, 40, 30, 30))) {
                result.AddRange(node.items.Select(e => e.data));
            }

            CollectionAssert.AreEquivalent(new[] { 1, 2 }, result);
        }
    }
}
