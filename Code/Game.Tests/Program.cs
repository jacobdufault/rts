﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game {
    class Program {
        private static string GetLevel(Rect objBounds, int quadScaleX, int quadScaleY, int totalLevels) {
            int x1 = objBounds.left / quadScaleX;
            int y1 = objBounds.top / quadScaleY;

            int xResult = x1 ^ (objBounds.right / quadScaleX);
            int yResult = y1 ^ (objBounds.bottom / quadScaleY);

            int nodeLevel = totalLevels;
            int shiftCount = 0;

            // Count highest bit position
            while (xResult + yResult != 0) {
                xResult >>= 1;
                yResult >>= 1;
                nodeLevel--;
                shiftCount++;
            }

            // Now lookup the node pointer in a 2D array stored linearly
            // Scale coordinates for quadtree level
            x1 >>= shiftCount;
            y1 >>= shiftCount;

            return $"NodeLevel={nodeLevel}, x1={x1}, y1={y1}, shiftCount={shiftCount}";

            //QuadNode[] nodes = nodeLevels[nodeLevel];
            //return ref (nodes[y1 << (nodeLevel - 1) + x1]);
        }

        // NOTES:
        //   Level0 => top/biggest
        //   LevelN => smallest
        //
        //   If nodeLevel goes negative the objBounds is not in the quadtree.

        private static int TotalAxisSizeAtLevel(int level, int totalLevels, int scale) {
            // Equivalent to 
            //   scale * (int)Math.Pow(2, totalLevels - level - 1)
            return scale * 1 << (totalLevels - level - 1);
        }

        private static int GetNumNodesPerAxis(int level, int totalLevels) {
            // Equivalent to 
            //   (int)Math.Pow(2, level)
            return 1 << level;
        }

        private static int GetArraySize(int level, int totalLevels) {
            int numNodes = GetNumNodesPerAxis(level, totalLevels);
            return numNodes * numNodes;
        }

        public static void Main2(string[] args) {
            int quadScaleX = 1;
            int quadScaleY = 1;
            int numLevels = 9;
            for (int lvl = 0; lvl < numLevels; ++lvl) {
                Console.WriteLine($" L{lvl} NodeDimension => {TotalAxisSizeAtLevel(lvl, numLevels, quadScaleX)}x{TotalAxisSizeAtLevel(lvl, numLevels, quadScaleY)},\tArraySize => {GetNumNodesPerAxis(lvl, numLevels)}x{GetNumNodesPerAxis(lvl, numLevels)}={GetArraySize(lvl, numLevels)}");

                var transitionPoints = new List<float>();
                for (int j = 0; j < GetNumNodesPerAxis(lvl, numLevels) / 2; ++j) {
                    int totalWidth = TotalAxisSizeAtLevel(lvl, numLevels, quadScaleX);
                    int numNodes = GetNumNodesPerAxis(lvl, numLevels);
                    float eachWidth = totalWidth / (float)numNodes;
                    transitionPoints.Add(eachWidth * j);
                }
                Console.WriteLine("   TransitionPoints: " + String.Join(", ", transitionPoints.Select(t => t.ToString()).ToArray()));
            }

            Console.ReadKey();
        }

        public static void Main(string[] args) {
            int quadScaleX = 5;
            int quadScaleY = 5;

            // 1 total levels: L0 is 1x1
            // 2 total levels: L0 is 2x2
            // 3 total levels: L0 is 4x4
            // 4 total levels: L0 is 8x8
            // 5 total levels: L0 is 16x16

            for (int numLevels = 1; numLevels < 7; ++numLevels) {
                var rect = new Rect(6, 1, 7, 2);

                Console.WriteLine($"Total levels: {numLevels}, scale: ({quadScaleX}, {quadScaleY})");
                for (int i = 0; i < numLevels; ++i) {
                    Console.WriteLine($" Dimensions of L{i} => {TotalAxisSizeAtLevel(i, numLevels, quadScaleX)}x{TotalAxisSizeAtLevel(i, numLevels, quadScaleY)},\tArraySize => {GetArraySize(i, numLevels)}");
                }

                //Console.WriteLine($"Total area at L0 should be { Math.Pow(2, numLevels - 1) }");
                Console.WriteLine($" {rect} is at {GetLevel(rect, quadScaleX, quadScaleY, numLevels)}");
                Console.WriteLine();
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
