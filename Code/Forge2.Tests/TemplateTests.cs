﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Forge2 {
    public class TemplateTests : CommonTestFixture {
        // We need to add DummyFrame so that we have an initializer registered for |int|.
        class DummyFrame : DenseFrame<int> { }
        protected override void SetupBuilder(Core.Builder builder) {
            builder.frames.Add<DummyFrame>();
        }

        [Test]
        public void AddingInvalidDataThrowsException() {
            Assert.DoesNotThrow(() => {
                var template = Template.Build().Add(0);
                entityManager.Create(template);
                core.Update();
            });

            Assert.Throws<InvalidOperationException>(() => {
                var template = Template.Build().Add(true);
                entityManager.Create(template);
                core.Update();
            });
        }

        [Test]
        public void CannotAddOverrideNotPresentInOriginal() {
            Assert.Throws<InvalidOperationException>(() => {
                Template template = Template.Build().Add(3);
                template.Override().Add(true);
            });
        }
    }
}
