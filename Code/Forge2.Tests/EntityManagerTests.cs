﻿using System.Diagnostics;
using NUnit.Framework;

namespace Forge2 {
    public class EntityManagerTests : CommonTestFixture {
        [Test]
        public void InitialEntityIdsStartFromZero() {
            // This also tests the resizing logic.
            for (int i = 0; i < 100000; ++i)
                Assert.AreEqual(i, entityManager.Create(emptyTemplate).id);
        }

        [Test]
        public void DestroyedEntityIdGetsReused() {
            Assert.AreEqual(0, entityManager.Create(emptyTemplate).id);
            Assert.AreEqual(1, entityManager.Create(emptyTemplate).id);
            Assert.AreEqual(2, entityManager.Create(emptyTemplate).id);
            Assert.AreEqual(3, entityManager.Create(emptyTemplate).id);

            entityManager.Destroy(new Entity(2));
            entityManager.Destroy(new Entity(3));
            Assert.AreEqual(4, entityManager.Create(emptyTemplate).id);

            Update();

            Assert.AreEqual(2, entityManager.Create(emptyTemplate).id);
            Assert.AreEqual(3, entityManager.Create(emptyTemplate).id);
        }

        [Test]
        public void CreateDestroyInSameCycle() {
            Assert.AreEqual(0, entityManager.Create(emptyTemplate).id);
            entityManager.Destroy(new Entity(0));

            Update();
            Assert.AreEqual(0, entityManager.Create(emptyTemplate).id);
        }

        [Test]
        public void EntityManagerOverheadIsConstant() {
            for (int i = 0; i < 100000; ++i)
                Assert.AreEqual(i, entityManager.Create(emptyTemplate).id);
            // Do not consider the time to allocate the entities.
            Update();

            // Run loop with nothing to do. This should take a relatively constant amount of time.
            for (int i = 0; i < 20; ++i) {
                var stopwatch = Stopwatch.StartNew();
                Update();
                stopwatch.Stop();
                Assert.Less(stopwatch.ElapsedTicks, 10000);
            }
        }
    }
}
