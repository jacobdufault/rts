﻿using System;
using Forge2.Internal;
using NUnit.Framework;

namespace Forge2 {
    public class MinHeapTests {
        [Test]
        public void TestBasicFunction() {
            DoTest(1);
            DoTest(1, 1);
            DoTest(1, 2, 3);
            DoTest(3, 2, 1);
            DoTest(1, 3, 2);
            DoTest(25, 0, 32, 3, 2);
            DoTest(93, 33, 89, 77, 50,
                   80, 45, 58, 89, 47,
                   40, 49, 84, 59, 77,
                   22, 71, 55, 13, 71,
                   59, 83, 17, 57, 68,
                   41, 60, 100, 31, 82,
                   19, 41, 15, 63, 66,
                   70, 1, 34, 10, 85,
                   26, 22, 48, 34, 75,
                   25, 59, 97, 85, 47,
                   1, 34, 58, 64, 69,
                   65, 29, 92, 18, 84,
                   15, 83, 57, 17, 84,
                   52, 13, 70, 28, 11,
                   44, 14, 90, 75, 37,
                   87, 24, 79, 38, 86,
                   9, 25, 16, 66, 48,
                   3, 19, 18, 18, 46,
                   72, 41, 65, 63, 9,
                   27, 27, 17, 39, 71);
        }

        private void DoTest<T>(params T[] elements) where T : struct, IComparable<T> {
            Test1(elements);
            Test2(elements);
        }

        private void Test1<T>(params T[] elements) where T : struct, IComparable<T> {
            var heap = new MinHeap<T>();
            foreach (var element in elements)
                heap.Add(element);

            Array.Sort(elements);
            foreach (var element in elements)
                Assert.AreEqual(element, heap.PopMin());
        }

        private void Test2<T>(params T[] elements) where T : struct, IComparable<T> {
            // Basically, this adds the first half of the elements. Then it verifies
            // the first 1/4th. Then it adds the rest of the elements, and verifies
            // them all.
            //
            // It tests inserting into the heap while using it.

            var heap = new MinHeap<T>();

            for (int idx = 0; idx < elements.Length / 2; ++idx)
                heap.Add(elements[idx]);

            Array.Sort(elements, 0, elements.Length / 2);

            // Check first fourth of the elements.
            for (int idx = 0; idx < elements.Length / 4; ++idx)
                Assert.AreEqual(elements[idx], heap.PopMin());

            // Add rest.
            for (int idx = elements.Length / 2; idx < elements.Length; ++idx)
                heap.Add(elements[idx]);

            Array.Sort(elements, elements.Length / 4, elements.Length - elements.Length / 4);

            // Check last elements.
            for (int idx = elements.Length / 4; idx < elements.Length; ++idx)
                Assert.AreEqual(elements[idx], heap.PopMin());
        }
    }
}
