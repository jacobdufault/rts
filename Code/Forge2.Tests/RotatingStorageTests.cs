﻿using NUnit.Framework;

namespace Forge2 {
    public class RotatingStorageTests {
        [Test]
        public void TestRotation() {
            int nextId = 0;
            var storage = new RotatingStorage<int>(3, () => nextId++);
            CollectionAssert.AreEqual(new int[] { 0, 1, 2 }, storage.items);

            Assert.AreEqual(0, storage.previous);
            Assert.AreEqual(1, storage.current);

            storage.Rotate();

            Assert.AreEqual(1, storage.previous);
            Assert.AreEqual(2, storage.current);

            storage.Rotate();

            Assert.AreEqual(2, storage.previous);
            Assert.AreEqual(0, storage.current);

            storage.Rotate();

            Assert.AreEqual(0, storage.previous);
            Assert.AreEqual(1, storage.current);
        }
    }
}
