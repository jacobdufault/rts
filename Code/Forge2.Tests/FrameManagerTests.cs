﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Forge2 {
    public class FrameManagerTests : CommonTestFixture {
        public class DenseVersionedBoolFrame : DenseVersionedFrame<bool> { }
        public class SparseVersionedBoolFrame : SparseVersionedFrame<bool> { }
        public class DenseVersionedIntFrameNotCopied : DenseVersionedFrame<int> {
            protected override bool InitializableWritableWithReadable => false;
        }
        public class SparseVersionedIntFrameNotCopied : SparseVersionedFrame<int> {
            protected override bool InitializableWritableWithReadable => false;
        }
        public class DenseBoolFrame : DenseFrame<bool> { }
        public class SparseBoolFrame : SparseFrame<bool> { }

        private DenseVersionedBoolFrame denseBoolVersioned;
        private SparseVersionedBoolFrame sparseBoolVersioned;
        private DenseBoolFrame denseBool;
        private SparseBoolFrame sparseBool;
        private DenseVersionedIntFrameNotCopied denseIntVersioned;
        private SparseVersionedIntFrameNotCopied sparseIntVersioned;

        private Template boolTemplate;
        private Template intTemplate;

        protected override void SetupBuilder(Core.Builder builder) {
            builder.frames
                .Add<DenseVersionedBoolFrame>()
                .Add<SparseVersionedBoolFrame>()
                .Add<DenseVersionedIntFrameNotCopied>()
                .Add<SparseVersionedIntFrameNotCopied>()
                .Add<DenseBoolFrame>()
                .Add<SparseBoolFrame>();

            boolTemplate = Template.Build().Add(true);
            intTemplate = Template.Build().Add(1);
        }

        protected override void OnPostInitialize() {
            denseBoolVersioned = frameManager.GetFrame<DenseVersionedBoolFrame>();
            sparseBoolVersioned = frameManager.GetFrame<SparseVersionedBoolFrame>();
            denseBool = frameManager.GetFrame<DenseBoolFrame>();
            sparseBool = frameManager.GetFrame<SparseBoolFrame>();
            denseIntVersioned = frameManager.GetFrame<DenseVersionedIntFrameNotCopied>();
            sparseIntVersioned = frameManager.GetFrame<SparseVersionedIntFrameNotCopied>();
        }

        [Test]
        public void TestGetFrame() {
            Assert.IsInstanceOf<DenseBoolFrame>(frameManager.GetFrame<DenseBoolFrame>());
        }

        [Test]
        public void TestVersionedFrame() {
            denseBoolVersioned.writable.storage.Set(new Entity(0), true);
            Assert.AreEqual(denseBoolVersioned.writable[new Entity(0)], true);
            Assert.IsFalse(denseBoolVersioned.readable.storage.Has(new Entity(0)));

            Update();

            Assert.AreEqual(denseBoolVersioned.readable[new Entity(0)], true);
            Assert.AreEqual(denseBoolVersioned.writable[new Entity(0)], true);
        }

        [Test]
        public void ReadableWritableAreNotSame() {
            var denseVersioned = frameManager.GetFrame<DenseVersionedBoolFrame>();
            Assert.AreNotEqual(denseVersioned.writable, denseVersioned.readable);
        }

        [Test]
        public void FramesPickUpInitData() {
            var boolEntity = entityManager.Create(boolTemplate);
            var intEntity = entityManager.Create(intTemplate);

            // Entities are added only after an update.
            Assert.IsFalse(denseBool.storage.Has(boolEntity));
            Update();
            Assert.IsTrue(denseBool.storage.Has(boolEntity));

            // int frames do not get bool entity, bool frames do not get int entity.
            Assert.IsFalse(denseBoolVersioned.readable.storage.Has(intEntity));
            Assert.IsFalse(denseBoolVersioned.writable.storage.Has(intEntity));
            Assert.IsFalse(sparseBoolVersioned.readable.storage.Has(intEntity));
            Assert.IsFalse(sparseBoolVersioned.writable.storage.Has(intEntity));
            Assert.IsFalse(denseBool.storage.Has(intEntity));
            Assert.IsFalse(sparseBool.storage.Has(intEntity));
            Assert.IsFalse(denseIntVersioned.readable.storage.Has(boolEntity));
            Assert.IsFalse(denseIntVersioned.writable.storage.Has(boolEntity));
            Assert.IsFalse(sparseIntVersioned.readable.storage.Has(boolEntity));
            Assert.IsFalse(sparseIntVersioned.writable.storage.Has(boolEntity));

            // Writable init data is copied as expected.
            Assert.AreEqual(true, denseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(true, denseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(true, sparseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(true, sparseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(true, denseBool[boolEntity]);
            Assert.AreEqual(true, sparseBool[boolEntity]);

            // Non-writable init data is not copied.
            Assert.AreEqual(1, denseIntVersioned.readable[intEntity]);
            Assert.AreEqual(0, denseIntVersioned.writable[intEntity]);
            Assert.AreEqual(1, sparseIntVersioned.readable[intEntity]);
            Assert.AreEqual(0, sparseIntVersioned.writable[intEntity]);
        }

        [Test]
        public void WritingDoesNotChangeReadable() {
            var boolEntity = entityManager.Create(boolTemplate);
            var intEntity = entityManager.Create(intTemplate);
            Update();

            Assert.AreEqual(true, denseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(true, denseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(true, sparseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(true, sparseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(1, denseIntVersioned.readable[intEntity]);
            Assert.AreEqual(0, denseIntVersioned.writable[intEntity]);
            Assert.AreEqual(1, sparseIntVersioned.readable[intEntity]);
            Assert.AreEqual(0, sparseIntVersioned.writable[intEntity]);


            denseBoolVersioned.writable[boolEntity] = false;
            sparseBoolVersioned.writable[boolEntity] = false;
            denseIntVersioned.writable[intEntity] = 2;
            sparseIntVersioned.writable[intEntity] = 2;


            Assert.AreEqual(true, denseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(false, denseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(true, sparseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(false, sparseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(1, denseIntVersioned.readable[intEntity]);
            Assert.AreEqual(2, denseIntVersioned.writable[intEntity]);
            Assert.AreEqual(1, sparseIntVersioned.readable[intEntity]);
            Assert.AreEqual(2, sparseIntVersioned.writable[intEntity]);

            Update();

            Assert.AreEqual(false, denseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(false, denseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(false, sparseBoolVersioned.readable[boolEntity]);
            Assert.AreEqual(false, sparseBoolVersioned.writable[boolEntity]);
            Assert.AreEqual(2, denseIntVersioned.readable[intEntity]);
            Assert.AreEqual(1, denseIntVersioned.writable[intEntity]);
            Assert.AreEqual(2, sparseIntVersioned.readable[intEntity]);
            Assert.AreEqual(1, sparseIntVersioned.writable[intEntity]);
        }
    }
}
