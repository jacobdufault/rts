﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Forge2 {
    public class EntityDictionaryStorageTests {
        private static List<KeyValuePair<Entity, bool>> ListOf(params int[] entityIds) {
            var result = new List<KeyValuePair<Entity, bool>>();
            foreach (var id in entityIds)
                result.Add(Entry(id, true));
            return result;
        }

        private static KeyValuePair<Entity, bool> Entry(int entityId, bool value) {
            return new KeyValuePair<Entity, bool>(new Entity(entityId), value);
        }

        private static IEnumerable<int> NaturalNumbers(int count) {
            for (int i = 0; i < count; ++i)
                yield return i;
        }

        private static IEnumerable<int> RandomNumbers(int count) {
            const int MAX = 1000;

            var random = new Random(50);
            
            var numbers = new HashSet<int>();
            while (numbers.Count != count)
                numbers.Add(random.Next(MAX));
            return numbers;
        }

        [DatapointSource]
        int[][] entityIds = new[] {
            new int[0],
            new[] { 0 },
            new[] { 0, 1, 2, 3, 4 },
            new[] { 0, 50, 3 },
            new[] { 0, 10, 20, 50000 },
            NaturalNumbers(100).ToArray(),
            RandomNumbers(100).ToArray()
        };

        [Datapoint]
        IEntityDictionary<bool> sparse = new SparseEntityDictionary<bool>();
        [Datapoint]
        IEntityDictionary<bool> dense = new DenseEntityDictionary<bool>();

        [Theory]
        public void Iteration(IEntityDictionary<bool> storage, int[] entityIds) {
            var items = ListOf(entityIds);

            // Add everything
            foreach (var item in items)
                storage.Add(item);
            CollectionAssert.AreEquivalent(items, storage);

            // Remove items
            for (int i = 0; i < items.Count; ++i) {
                storage.Remove(items[i].Key);

                // Convert to a HashSet and use SetEquals since it is
                // significantly faster than CollectionAssert.AreEquivalent
                var storageHash = new HashSet<KeyValuePair<Entity, bool>>(storage);
                Assert.IsTrue(storageHash.SetEquals(items.Skip(i + 1)));
            }
        }
    }
}
