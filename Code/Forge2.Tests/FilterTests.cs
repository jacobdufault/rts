﻿using System;
using NUnit.Framework;

namespace Forge2 {
    public class FilterTests : CommonTestFixture {
        struct DataA {
            public class FrameStorage : DenseFrame<DataA> { }
        }
        struct DataB {
            public class FrameStorage : DenseFrame<DataB> { }
        }

        public class BoolFrameWithIntInit : DenseFrame<bool, int> {
            protected override void Init(Entity entity, int initData, ref bool frameData) {
                frameData = initData > 0;
            }
        }

        private Template templateBlank;
        private Template templateA;
        private Template templateB;
        private Template templateAB;

        protected override bool DeferredInitialize => true;

        protected override void SetupBuilder(Core.Builder builder) {
            templateBlank = Template.Build();
            templateA = Template.Build().Add(new DataA());
            templateB = Template.Build().Add(new DataB());
            templateAB = Template.Build().Add(new DataA()).Add(new DataB());

            builder.frames
                .Add<DataA.FrameStorage>()
                .Add<DataB.FrameStorage>()
                .Add<BoolFrameWithIntInit>();
        }

        [Test]
        public void DifferentInitAndDataTypes() {
            Filter filter = Filter.AllOf<bool>();
            Initialize(filter);

            Entity e = entityManager.Create(Template.Build().Add(3));
            Update();

            CollectionAssert.AreEqual(new[] { e }, filter.entities);
        }

        [Test]
        public void ThrowsExceptionOnUnknownDataType() {
            Assert.Throws<InvalidOperationException>(() => {
                Filter filter = Filter.AllOf<string>();
                Initialize(filter);
            });
        }

        [Test]
        public void TestBlankFilter() {
            var filter = Filter.Empty();
            Initialize(filter);

            entityManager.Create(templateA);
            entityManager.Create(templateB);
            entityManager.Create(templateAB);

            Update();

            Assert.AreEqual(3, filter.entities.Length);
        }

        [Test]
        public void TestAllFilter() {
            Filter filter = Filter.AllOf<DataA, DataB>();
            Initialize(filter);

            var eA = entityManager.Create(templateA);
            var eB = entityManager.Create(templateB);
            var eAB = entityManager.Create(templateAB);

            Update();

            CollectionAssert.DoesNotContain(filter.entities, eA);
            CollectionAssert.DoesNotContain(filter.entities, eB);
            CollectionAssert.Contains(filter.entities, eAB);
        }

        [Test]
        public void TestOrFilter() {
            Filter filter = Filter.AnyOf<DataA, DataB>();
            Initialize(filter);

            entityManager.Create(templateA);
            entityManager.Create(templateB);
            entityManager.Create(templateAB);

            Update();

            Assert.AreEqual(3, filter.entities.Length);
        }

        [Test]
        public void TestNotFilter() {
            Filter filter = Filter.NoneOf<DataA>();
            Initialize(filter);

            var eA = entityManager.Create(templateA);
            var eB = entityManager.Create(templateB);
            var eAB = entityManager.Create(templateAB);

            Update();

            CollectionAssert.DoesNotContain(filter.entities, eA);
            CollectionAssert.Contains(filter.entities, eB);
            CollectionAssert.DoesNotContain(filter.entities, eAB);
        }

        [Test]
        public void TestNestedFilter() {
            // It needs to have either a or b, but not both.
            Filter filter = Filter
                .AnyOf<DataA, DataB>()
                .IsFalse(Filter.AllOf<DataA, DataB>());
            Initialize(filter);

            var eBlank = entityManager.Create(templateBlank);
            var eA = entityManager.Create(templateA);
            var eB = entityManager.Create(templateB);
            var eAB = entityManager.Create(templateAB);

            Update();

            CollectionAssert.DoesNotContain(filter.entities, eBlank);
            CollectionAssert.Contains(filter.entities, eA);
            CollectionAssert.Contains(filter.entities, eB);
            CollectionAssert.DoesNotContain(filter.entities, eAB);
        }
    }
}
