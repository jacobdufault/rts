﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2 {
    public class LinearAllocator<T> {
        private T[] data_;
        private int length_;

        public LinearAllocator(int capacity = 64) {
            data_ = new T[capacity];
            length_ = 0;
        }

        public ref T Allocate() {
            length_ += 1;
            if (length_ > data_.Length)
                Array.Resize(ref data_, data_.Length * 2);

            return ref data_[length_ - 1];
        }

        public void Allocate(T value) {
            Allocate() = value;
        }

        public void Clear() {
            length_ = 0;
        }

        public Enumerator GetEnumerator() => new Enumerator(this);

        public struct Enumerator : IEnumerator<T> {
            private LinearAllocator<T> allocator_;
            private int index_;

            public Enumerator(LinearAllocator<T> allocator) {
                allocator_ = allocator;
                index_ = -1;
            }

            object IEnumerator.Current => Current;
            public T Current => allocator_.data_[index_];

            public bool MoveNext() {
                index_ += 1;
                return index_ < allocator_.length_;
            }

            public void Dispose() {
                allocator_ = null;
                index_ = -1;
            }

            public void Reset() {
                index_ = -1;
            }
        }
    }
}
