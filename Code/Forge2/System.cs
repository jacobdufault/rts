﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Forge2 {
    public abstract class BaseSystem {
        protected EntityManager entityManager;
        protected FrameManager frameManager;

        protected T GetFrame<T>() where T : IFrame {
            return frameManager.GetFrame<T>();
        }

        public void Initialize(EntityManager entityManager, FrameManager frameManager) {
            this.entityManager = entityManager;
            this.frameManager = frameManager;
        }
        protected virtual void OnInitialized() { }

        public abstract void Update();
    }

    public class SystemManager {
        public BaseSystem[] allSystems;

        internal SystemManager(BaseSystem[] systems) {
            allSystems = systems;
        }

        public IEnumerable<Filter> allFilters => from system in allSystems
                                                 from filter in ReflectionUtils.FindFiltersOnObject(system)
                                                 select filter;

        public T GetSystem<T>() where T : BaseSystem {
            foreach (BaseSystem system in allSystems) {
                if (system is T)
                    return (T)system;
            }

            throw new InvalidOperationException($"No system registered with type {typeof(T)}");
        }

        public void Initialize(EntityManager entityManager, FrameManager frameManager) {
            foreach (BaseSystem system in allSystems)
                system.Initialize(entityManager, frameManager);
        }

        internal void Update() {
            foreach (BaseSystem system in allSystems)
                system.Update();
        }

        public class Builder {
            private List<BaseSystem> systems_ = new List<BaseSystem>();

            public Builder Add<T>() where T : BaseSystem, new() {
                systems_.Add(new T());
                return this;
            }

            public SystemManager Build() {
                return new SystemManager(systems_.ToArray());
            }
        }
    }
}
