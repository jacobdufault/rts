﻿using System.Collections.Generic;
using System.Linq;
using Forge2.Internal;

namespace Forge2 {

    public interface IEntityCreated {
        void OnCreated(Entity entity, Template template, Template.Overrides overrides);
    }
    public interface IEntityDestroyed {
        void OnDestroyed(List<Entity> destroyed);
    }

    public class EntityManager {
        private HashSet<Entity> alive_ = new HashSet<Entity>();
        private MinHeap<Entity> dead_ = new MinHeap<Entity>();
        private IEntityCreated[] created_;
        private IEntityDestroyed[] destroyed_;
        private FilterManager filterManager_;
        private FrameManager frameManager_;
        private IInitReceiver[] initReceivers_;

        private struct DeferredCreation {
            public Entity entity;
            public Template template;
            public Template.Overrides overrides;
        }
        private Buffered<List<DeferredCreation>> toCreate_ = Buffered.AutoCreate<List<DeferredCreation>>();
        private Buffered<List<Entity>> toDestroy_ = Buffered.AutoCreate<List<Entity>>();

        public int AliveCount => alive_.Count;

        public EntityManager(IInitReceiver[] initReceivers, FrameManager frameManager, FilterManager filterManager) {
            frameManager_ = frameManager;
            filterManager_ = filterManager;
            initReceivers_ = initReceivers;

            created_ = (new[] { filterManager }).Concat(frameManager.frames.OfType<IEntityCreated>()).ToArray();
            destroyed_ = (new[] { filterManager }).Concat(frameManager.frames.OfType<IEntityDestroyed>()).ToArray();
        }

        public Entity Create(Template template) {
            return Create(template, null);
        }

        public Entity Create(Template.Overrides overrides) {
            return Create(overrides.template_, overrides);
        }

        private Entity Create(Template template, Template.Overrides overrides) {
            // Try to reuse a dead entity.
            Entity entity = dead_.Empty ? new Entity(alive_.Count) : dead_.PopMin();
            alive_.Add(entity);
            toCreate_.active.Add(new DeferredCreation {
                entity = entity,
                template = template,
                overrides = overrides
            });

            return entity;
        }

        internal void RunCreateDestroy() {
            toCreate_.Swap();
            toDestroy_.Swap();

            // Create
            foreach (DeferredCreation create in toCreate_.inactive) {
                create.template.RunAllocators(initReceivers_, create.entity, create.overrides);
                foreach (IEntityCreated callback in created_)
                    callback.OnCreated(create.entity, create.template, create.overrides);
            }
            toCreate_.inactive.Clear();

            // Destroy
            foreach (IEntityDestroyed callback in destroyed_)
                callback.OnDestroyed(toDestroy_.inactive);
            foreach (var destroyed in toDestroy_.inactive) {
                alive_.Remove(destroyed);
                dead_.Add(destroyed);
            }
            toDestroy_.inactive.Clear();
        }

        public void Destroy(Entity entity) {
            toDestroy_.active.Add(entity);
        }

        /*
         * // TODO: Maybe this makes more sense instead of dynamically resizing dictionaries / etc?
        public void Resize(int entityCount) {
            int startingCapacity = alive.Count + dead.Count;
            if (entityCount < startingCapacity)
                throw new ArgumentException("Cannot shrink");

            foreach (IFrame frame in frameManager_.frames) {
                var storage = frame as IEntityLifecycle;
                if (storage != null)
                    storage.Resize(entityCount);
            }

            for (int i = entityCount - 1; i >= startingCapacity; --i)
                dead.Add(new Entity { id = i });
        }*/

        /*
        public void Optimize() {
            // We always sort |alive| so that we remove the highest values.
            alive.Sort();

            // We want to take alive entities from the back of alive, so that we move everything
            // closer together in memory. We want to take dead from the front for the same reason.
            // Basically, we are trying to remove the gaps from the start of the storage arrays.
            int aliveIndex = alive.Count - 1;
            int numToMove = Math.Min(dead.Count, alive.Count);
            for (int i = 0; i < numToMove; ++i) {
                Entity deadEntity = dead.PopMin();
                ExecuteCopy(from: new Entity { id = aliveIndex }, to: deadEntity);
                aliveIndex -= 1;
            }

            // Alive is a linear array.
            for (int i = 0; i > alive.Count; ++i)
                alive[i] = new Entity { id = i };
        }
        */
        /*
        private void ExecuteCopy(Entity from, Entity to) {
            foreach (var storage_ in frameManager_.writableFrame) {
                var storage = storage_ as IEntityStorage;
                if (storage != null)
                    storage.Copy(from, to);
            }
        }*/
    }
}
