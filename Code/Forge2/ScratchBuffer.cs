﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2 {
    public static class ScratchBuffer {
        public static ScratchBuffer<T> Get<T>(int capacity) {
            return new ScratchBuffer<T>(capacity);
        }
    }

    public struct ScratchBuffer<T> : IDisposable {
        public static class Cache {
            [ThreadStatic]
            private static readonly List<IList> buffers = new List<IList>();

            public static List<T> Take(int capacity) {
                // Try to fetch a buffer that already has enough capacity.
                for (int i = buffers.Count - 1; i >= 0; --i) {
                    var buffer = (List<T>)buffers[i];
                    if (buffer.Capacity >= capacity) {
                        buffers.RemoveAt(i);
                        return buffer;
                    }
                }

                // Reuse an existing buffer, even if it doesn't have enough capacity.
                if (buffers.Count > 0) {
                    var buffer = (List<T>)buffers[buffers.Count - 1];
                    buffers.RemoveAt(buffers.Count - 1);
                    buffer.Capacity = capacity;
                    return buffer;
                }

                // Allocate.
                return new List<T>(capacity);
            }

            public static void Return(ref List<T> buffer) {
                buffer.Clear();
                buffers.Add(buffer);
                buffer = null;
            }
        }

        public List<T> buffer;

        public ScratchBuffer(int capacity = 0) {
            buffer = Cache.Take(capacity);
        }

        public void Add(T value) {
            buffer.Add(value);
        }

        public List<T>.Enumerator GetEnumerator() {
            return buffer.GetEnumerator();
        }

        public void Dispose() {
            Cache.Return(ref buffer);
        }
    }
}
