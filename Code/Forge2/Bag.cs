﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2 {
    /// <summary>
    /// An unordered collection of items.
    /// </summary>
    public sealed class Bag<T> : IEnumerable<T>, IEnumerable {
        private T[] items_;

        public int Length {
            get;
            private set;
        }

        public Bag()
            : this(8) {
        }

        public Bag(int capacity) {
            items_ = new T[capacity];
            Length = 0;
        }

        /// <summary>
        /// Copies from the given bag into this one.
        /// </summary>
        /// <param name="bag">The bag to copy from</param>
        public void CopyFrom(Bag<T> bag) {
            // Increase capacity if necessary
            if (bag.Length > items_.Length) {
                Array.Resize(ref items_, bag.Length);
            }

            // Copy items over
            Array.Copy(bag.items_, items_, bag.Length);

            // Clear out old items
            if (Length > bag.Length) {
                Array.Clear(items_, bag.Length, Length - bag.Length);
            }

            // Update length
            Length = bag.Length;
        }

        /// <summary>
        /// Creates a duplicate of this bag that has a different backing array.
        /// </summary>
        public Bag<T> Copy() {
            var bag = new Bag<T>(items_.Length);
            Array.Copy(items_, bag.items_, Length);
            return bag;
        }

        /// <summary>
        /// Returns the index of the given item in the bag, or -1 if it is not found.
        /// </summary>
        public int IndexOf(T item) {
            return Array.IndexOf(items_, item, 0, Length);
        }

        /// <summary>
        /// Removes the item at given index from the bag in O(1) time. This operation does not
        /// maintain the order of elements inside of the bag!
        /// </summary>
        public void Remove(int index) {
            // swap
            items_[index] = items_[Length - 1];

            // reduce length
            items_[Length - 1] = default(T);
            --Length;
        }

        /// <summary>
        /// Clears all stored items from this instance.
        /// </summary>
        public void Clear() {
            Array.Clear(items_, 0, Length);
            Length = 0;
        }

        /// <summary>
        /// Remove the item from the bag. This is O(n) and has to scan the bag to find the item.
        /// </summary>
        /// <returns>True if the item was found and removed, false otherwise.</returns>
        public bool Remove(T item) {
            int index = IndexOf(item);
            if (index == -1) {
                return false;
            }

            Remove(index);
            return true;
        }

        public void Add(T value) {
            if (Length == items_.Length) {
                Array.Resize(ref items_, (int)((items_.Length + 1) * 1.5));
            }

            items_[Length++] = value;
        }

        public void AddRange(IEnumerable<T> values) {
            foreach (T value in values)
                Add(value);
        }

        public ref T this[int index] => ref items_[index];

        /// <summary>
        /// Returns true if the Bag contains an instance of the given item.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <returns>True if it is in the bag, false otherwise.</returns>
        public bool Contains(T item) {
            return IndexOf(item) >= 0;
        }

        IEnumerator IEnumerable.GetEnumerator() => new Enumerator(this);
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => new Enumerator(this);
        public Enumerator GetEnumerator() => new Enumerator(this);

        public struct Enumerator : IEnumerator<T> {
            private Bag<T> bag_;
            private int index_;

            public Enumerator(Bag<T> bag) {
                bag_ = bag;
                index_ = -1;
            }

            object IEnumerator.Current => Current;
            public T Current => bag_.items_[index_];

            public bool MoveNext() {
                index_ += 1;
                return index_ < bag_.Length;
            }

            public void Reset() {
                index_ = -1;
            }

            void IDisposable.Dispose() { }
        }
    }
}