﻿using System;

namespace Forge2 {
    public class RotatingStorage<T> {
        public T[] items;

        // It is easier/faster to just use two variables instead of storing
        // previousId and computing currentId as
        // (previousId + 1) % items.Length.
        private int currentId;
        private int previousId;

        public RotatingStorage(int count, Func<T> allocator) {
            previousId = 0;
            currentId = 1;

            items = new T[count];
            for (int i = 0; i < items.Length; ++i)
                items[i] = allocator();
        }

        public T current {
            get { return items[currentId]; }
        }

        public T previous {
            get { return items[previousId]; }
        }

        public void Rotate() {
            previousId = (previousId + 1) % items.Length;
            currentId = (currentId + 1) % items.Length;
        }
    }

}
