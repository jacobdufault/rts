﻿using System;
using System.Collections.Generic;

namespace Forge2 {
    /*
    public class InitLookupCache {
        private Dictionary<Type, IInitializable[]> initDataToInitializer;

        public InitLookupCache(IEnumerable<IInitializable> initializers) {
            var dict = new Dictionary<Type, List<IInitializable>>();
            foreach (IInitializable initializer in initializers) {
                List<IInitializable> entries;
                if (!dict.TryGetValue(initializer.InitType, out entries)) {
                    entries = new List<IInitializable>();
                    dict[initializer.InitType] = entries;
                }
                entries.Add(initializer);
            }

            initDataToInitializer = new Dictionary<Type, IInitializable[]>();
            foreach (var entry in dict)
                initDataToInitializer[entry.Key] = entry.Value.ToArray();
        }

        private static IInitializable[] s_emptyInitializers = new IInitializable[0];

        public bool HasInitsForType(Type type) {
            return initDataToInitializer.ContainsKey(type);
        }

        public IInitializable[] GetInitsForType(Type type) {
            IInitializable[] result;
            if (initDataToInitializer.TryGetValue(type, out result))
                return result;
            return s_emptyInitializers;
        }
    }*/
}