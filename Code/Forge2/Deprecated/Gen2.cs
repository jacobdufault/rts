﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Forge2.Gen2 {
    public interface IEntityLifecycleListener {
        void OnCreated(Entity entity);
        void OnDestroyed(Entity entity);
    }
    public abstract class Filter : IEntityLifecycleListener {
        // TODO: Use a bag or something like it.
        public List<Entity> infilter;

        protected abstract bool WantsEntity(Entity entity);

        void IEntityLifecycleListener.OnCreated(Entity entity) {
            if (WantsEntity(entity))
                infilter.Add(entity);
        }

        void IEntityLifecycleListener.OnDestroyed(Entity entity) {
            if (!WantsEntity(entity))
                infilter.Remove(entity);
        }
    }

    public interface IHotColdListener {
        void Initialize(int hotStorageSize);
        void GrowHotStorage(int size);
        void MoveToHot(Entity coldToBecomeHot);
        void SwapHotCold(Entity coldToBecomeHot, Entity hotToBecomeCold);
    }

    public class HotManager {
        const int INITIAL_HOT_SIZE = 1024;

        private bool[] hot_;
        private List<Entity> wantsHot_;
        private IHotColdListener[] listeners_;

        public void MarkHot(Entity entity) {
            if (entity.id < hot_.Length) {
                hot_[entity.id] = true;
            }
            else {
                wantsHot_.Add(entity);
            }
        }

        public HotManager(IEnumerable<IHotColdListener> listeners) {
            wantsHot_ = new List<Entity>(INITIAL_HOT_SIZE);
            listeners_ = listeners.ToArray();

            hot_ = new bool[INITIAL_HOT_SIZE];
            foreach (var listener in listeners_)
                listener.Initialize(INITIAL_HOT_SIZE);
        }

        public void SwapHotAndColdAsNeeded() {
            int hotIdx = 0;
            int wantsHotIdx = 0;

            while (true) {
                // Find an entity in |hot| that was not accessed.
                while (hotIdx < hot_.Length && hot_[hotIdx])
                    ++hotIdx;
                if (hotIdx >= hot_.Length)
                    break;

                SwapEntities(new Entity(hotIdx), wantsHot_[wantsHotIdx]);

                wantsHotIdx += 1;
                if (wantsHotIdx >= wantsHot_.Count)
                    break;
            }

            // We did not have enough dead entities inside of |hot|; we need to expand storage.
            if (wantsHotIdx < wantsHot_.Count) {
                GrowHotStorage();
                for (; wantsHotIdx < wantsHot_.Count; ++wantsHotIdx)
                    MoveToHot(wantsHot_[wantsHotIdx]);
            }

            // Clear out existing hot state so we can move things to cold as needed.
            wantsHot_.Clear();
            Array.Clear(hot_, 0, hot_.Length);
        }

        private void MoveToHot(Entity coldToBecomeHot) {
            foreach (IHotColdListener listener in listeners_)
                listener.MoveToHot(coldToBecomeHot);
        }

        private void SwapEntities(Entity coldToBecomeHot, Entity hotToBecomeCold) {
            foreach (IHotColdListener listener in listeners_)
                listener.SwapHotCold(coldToBecomeHot, hotToBecomeCold);
        }

        private void GrowHotStorage() {
            Array.Resize(ref hot_, hot_.Length * 2);
            foreach (IHotColdListener listener in listeners_)
                listener.GrowHotStorage(hot_.Length);
        }
    }

    // TODO:
    //   We have two frames, readable and writable. Entity a existed in the last frame but does not
    //   exist in this frame anymore.
    //
    //   - The entity should be removed from readable as well, since it doesn't exist anymore. *OR*
    //   - The entity exists in readable, but does not point to anything in writable. The entity id
    //     will be reused in the next update.
    //
    // StaticPosition {
    //    
    // }
    //
    // DynamicPosition {
    //
    // }
    // 
    public class HotColdFrame<T> : IHotColdListener where T : struct {
        // TODO: The problem with this eviction strategy is that it involves an if every time we run.
        //       We could allocate an aux array the same size as Entity[] and from there just mark the
        //       bool set to true. No if, so it should be fast. When we want to check hot state we
        //       just clear the relevant part of the array.
        //
        // Do we even need a separation between hot/cold? Should we allow data to move between the two?
        // What about specify different arenas for data?
        //  entityManager.allocate(UsageHint.Inactive, TreeTemplate);
        //  entityManager.allocate(UsageHint.Active, TownCenterTemplate);
        //
        // We could instead do something like
        //  entityManager.MakeHot(100, ...) /* 100 frames of being in hot data store */
        // when we want to move something to hot data. We immediately move it to hot storage and
        // do a time-sliced update per frame, reducing the counter by x. After the entity hasn't
        // been hot for enough time we automatically move it back to cold.
        //
        // TODO: Would it be so bad to increment a counter every time we access an entity?
        //  -- note: 1,000,000 4 byte objects => 4 megabytes
        //
        // Also, instead of this hot/cold approach, we could have different systems for props/non-props.
        //  TreePosition, Position, etc.

        public bool[] hasHotData;
        public T[] hot;

        public Dictionary<Entity, int> entityToColdIndex;
        public T[] cold;

        void IHotColdListener.Initialize(Int32 hotStorageSize) {
            throw new NotImplementedException();
        }

        void IHotColdListener.GrowHotStorage(Int32 size) {
            throw new NotImplementedException();
        }

        void IHotColdListener.MoveToHot(Entity coldToBecomeHot) {
            throw new NotImplementedException();
        }

        void IHotColdListener.SwapHotCold(Entity coldToBecomeHot, Entity hotToBecomeCold) {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A bucket contains a fixed number of entities.
    /// </summary>
    public class Bucket<T> where T : struct {
        // Mapping from array index back into the entity the array is associated with.
        public Entity[] ids;
        // Actual data inside of the bucket.
        public T[] entities;
    }

    public class Frame<T> where T : struct {
        // Entity to bucket index. If the entity is not in the dictionary it is not in the Frame.
        public Dictionary<Entity, int> entities;
        // Data storage.
        public Bucket<T>[] buckets;

        // Use regular init system in existing Frame.
        // Provide automatic bucket compaction?
        // Provide bucket prefetch?
    }
}
