﻿using System;
using System.Collections.Generic;

namespace Forge2.Old {
    /// <summary>
    /// LinearFrameStorage will allocate every T datablock in a linear array,
    /// so that they can be iterated over extremely quickly. This means that
    /// to access the data for a specific entity, two lookups are required.
    /// <br />
    /// This class is best used when storage needs to be iterated very quickly
    /// in an entity-agnostic manner.
    /// </summary>
    public abstract class LinearFrameStorage<T, TInit> : BaseFrameStorage<T, TInit>, IFrameStorage
        where T : struct {

        /// <summary>
        /// If set to true, then the new writable state for the per-frame data
        /// will be initialized with the previous per-frame data.
        /// <br />
        /// This defaults to true.
        /// </summary>
        public virtual bool PreInitializeWritable => true;
        public virtual bool EnableDataIndexToEntity => true;

        public int[] entityToDataIndex = new int[0];
        // Reverse mapping that can be used to fetch the given entity that
        // owns a given data block. This is provided by default assuming
        // that the given frame storage has not disabled it.
        private int[] dataIndexToEntity_ = new int[0];
        public int[] dataIndexToEntity {
            get {
                if (!EnableDataIndexToEntity)
                    throw new InvalidOperationException("dataIndexToEntity is not available");
                return dataIndexToEntity_;
            }
        }
        public T[] linearData = new T[0];

        public sealed override ref T this[Entity entity] {
            get {
                var index = entityToDataIndex[entity.id];
                return ref linearData[index];
            }
        }

        void IFrameStorage.CopyTo(IFrameStorage other_) {
            var other = (LinearFrameStorage<T, TInit>)other_;
            entityToDataIndex.CopyTo(other.entityToDataIndex, 0);
            if (EnableDataIndexToEntity)
                dataIndexToEntity_.CopyTo(other.dataIndexToEntity_, 0);
            linearData.CopyTo(other.linearData, 0);
        }

        public virtual void OnDestroyed(List<Entity> entities) {
        }

        void Copy(Entity from, Entity to) {
            entityToDataIndex[to.id] = entityToDataIndex[from.id];
            if (EnableDataIndexToEntity)
                dataIndexToEntity_[to.id] = dataIndexToEntity_[from.id];
            linearData[to.id] = linearData[from.id];
        }

        void Resize(int entityCount) {
            Array.Resize(ref entityToDataIndex, entityCount);
            if (EnableDataIndexToEntity)
                Array.Resize(ref dataIndexToEntity_, entityCount);
            Array.Resize(ref linearData, entityCount);
        }
    }

    public class LinearFrameStorage<T> : LinearFrameStorage<T, T> where T : struct {
        protected override void Init(T initData, ref T frameData) {
            frameData = initData;
        }
    }
}
