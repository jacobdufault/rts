﻿using System;

namespace Forge2.Old {
    public interface IFrameStorage {
        void CopyTo(IFrameStorage other);

        /// <summary>
        /// If set to true, then the new writable state for the per-frame data
        /// will be initialized with the previous per-frame data.
        /// <br />
        /// This defaults to true.
        /// </summary>
        bool PreInitializeWritable { get; }
    }

    public abstract class BaseFrameStorage<T, TInit> : IInitReceiver<TInit> {
        public abstract ref T this[Entity entity] { get; }

        Type IInitReceiver.InitType => typeof(TInit);

        void IInitReceiver<TInit>.Init(Entity entity, TInit value) {
            ref T frameData = ref this[entity];
            Init(value, ref frameData);
        }

        protected abstract void Init(TInit initData, ref T frameData);
    }
}
