﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2.Internal {
    // TODO: Make this internal, expose to tests.
    // See http://allanrbo.blogspot.com/2011/12/simple-heap-implementation-priority.html
    public class MinHeap<T> : IEnumerable where T : struct, IComparable<T> {
        private List<T> data_ = new List<T>();

        public void Add(T data) {
            data_.Add(data);

            int i = data_.Count - 1;
            while (i > 0) {
                int j = (i + 1) / 2 - 1;

                // Check if the invariant holds for the element in data[i]  
                T v = data_[j];
                if (v.CompareTo(data_[i]) < 0 || v.CompareTo(data_[i]) == 0) {
                    break;
                }

                // Swap the elements  
                T tmp = data_[i];
                data_[i] = data_[j];
                data_[j] = tmp;

                i = j;
            }
        }

        public T PopMin() {
            if (data_.Count < 0) {
                throw new ArgumentOutOfRangeException();
            }

            T min = data_[0];
            data_[0] = data_[data_.Count - 1];
            data_.RemoveAt(data_.Count - 1);
            MinHeapify(0);
            return min;
        }

        public T PeekMin() {
            return data_[0];
        }

        public bool Empty => data_.Count == 0;
        public int Count => data_.Count;

        private void MinHeapify(int i) {
            int smallest;
            int left = 2 * (i + 1) - 1;
            int right = 2 * (i + 1);

            if (left < data_.Count && (data_[left].CompareTo(data_[i]) < 0)) {
                smallest = left;
            }
            else {
                smallest = i;
            }

            if (right < data_.Count && data_[right].CompareTo(data_[smallest]) < 0) {
                smallest = right;
            }

            if (smallest != i) {
                T tmp = data_[i];
                data_[i] = data_[smallest];
                data_[smallest] = tmp;
                MinHeapify(smallest);
            }
        }

        // This is only defined to provide access to C# {} style constructor.
        IEnumerator IEnumerable.GetEnumerator() {
            throw new NotImplementedException();
        }
    }
}
