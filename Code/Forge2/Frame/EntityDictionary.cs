using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2 {
    public static class IEntityDictionaryExtensions {
        public static void Add<T>(this IEntityDictionary<T> dict, KeyValuePair<Entity, T> kvp)
            where T : struct {
            dict.Set(kvp.Key, kvp.Value);
        }
        public static void Add<T>(this IEntityDictionary<T> dict, Entity entity, T value)
           where T : struct {
            dict.Set(entity, value);
        }
    }

    public interface IEntityDictionary<T> : IEnumerable<KeyValuePair<Entity, T>> where T : struct {
        // TODO: Tidy up this API.
        void Set(Entity entity, T value);
        void Remove(Entity entity);
        bool Has(Entity entity);
        bool TryGet(Entity entity, out T value);
        ref T GetOrInit(Entity entity);
        ref T this[Entity entity] { get; }
        void CopyTo(IEntityDictionary<T> dest);
    }

    /// <summary>
    /// Entity dictionary most useful when most of the entities have associated values.
    /// </summary>
    public class DenseEntityDictionary<T> : IEntityDictionary<T> where T : struct {
        private const int INITIAL_CAPACITY = 32;

        private T[] values_ = new T[INITIAL_CAPACITY];
        private bool[] has_ = new bool[INITIAL_CAPACITY];

        private void EnsureStorageFor(Entity entity) {
            while (entity.id >= values_.Length) {
                Array.Resize(ref values_, values_.Length * 2);
                Array.Resize(ref has_, has_.Length * 2);
            }
        }

        public int Length => values_.Length;
        public ref T this[Entity entity] => ref values_[entity.id];

        public void Set(Entity entity, T value) {
            GetOrInit(entity) = value;
        }

        public void Remove(Entity entity) {
            values_[entity.id] = default(T);
            has_[entity.id] = false;
        }

        public bool Has(Entity entity) {
            return has_[entity.id];
        }

        public bool TryGet(Entity entity, out T value) {
            if (entity.id > has_.Length || !has_[entity.id]) {
                value = default(T);
                return false;
            }

            value = values_[entity.id];
            return true;
        }

        public ref T GetOrInit(Entity entity) {
            EnsureStorageFor(entity);
            has_[entity.id] = true;
            return ref values_[entity.id];
        }

        public void CopyTo(IEntityDictionary<T> dest_) {
            var dest = (DenseEntityDictionary<T>)dest_;

            if (values_.Length > dest.values_.Length) {
                Array.Resize(ref dest.values_, values_.Length);
                Array.Resize(ref dest.has_, has_.Length);
            }
            Array.Copy(values_, dest.values_, values_.Length);
            Array.Copy(has_, dest.has_, has_.Length);
        }

        struct Enumerator : IEnumerator<KeyValuePair<Entity, T>> {
            private int index_;
            private DenseEntityDictionary<T> storage_;

            public Enumerator(DenseEntityDictionary<T> storage) {
                index_ = -1;
                storage_ = storage;
            }

            public KeyValuePair<Entity, T> Current {
                get {
                    return new KeyValuePair<Entity, T>(new Entity(index_), storage_.values_[index_]);
                }
            }
            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }

            bool IEnumerator.MoveNext() {
                index_++;
                if (index_ >= storage_.has_.Length)
                    return false;
                while (!storage_.has_[index_]) {
                    index_++;
                    if (index_ >= storage_.Length)
                        return false;
                }
                return true;
            }

            void IEnumerator.Reset() {
                index_ = -1;
            }
        }

        public IEnumerator<KeyValuePair<Entity, T>> GetEnumerator() {
            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return new Enumerator(this);
        }
    }

    /// <summary>
    /// Entity dictionary most useful when only some of the entities have values associated.
    /// </summary>
    public class SparseEntityDictionary<T> : IEntityDictionary<T> where T : struct {
        private int clusterSize_;
        private Bag<int> clusterIndices = new Bag<int>();
        private Cluster[] allClusters = new Cluster[0];

        public SparseEntityDictionary() : this(100) { }
        public SparseEntityDictionary(int clusterSize) {
            clusterSize_ = clusterSize;
        }

        public ref T this[Entity entity] {
            get {
                int clusterId = entity.id / clusterSize_;
                int localId = entity.id % clusterSize_;

                return ref allClusters[clusterId].data[localId];
            }
        }

        public void Remove(Entity entity) {
            int clusterId = entity.id / clusterSize_;
            int localId = entity.id % clusterSize_;

            if (clusterId >= allClusters.Length)
                return;

            Cluster cluster = allClusters[clusterId];
            if (cluster == null)
                return;

            // TODO: We could consider removing the cluster entirely.

            cluster.hasEntity[localId] = false;
            cluster.data[localId] = default(T);
        }

        public void Set(Entity entity, T data) {
            GetOrInit(entity) = data;
        }

        public bool TryGet(Entity entity, out T value) {
            int clusterId = entity.id / clusterSize_;
            int localId = entity.id % clusterSize_;

            if (clusterId >= allClusters.Length) {
                value = default(T);
                return false;
            }
            Cluster cluster = allClusters[clusterId];
            if (cluster == null) {
                value = default(T);
                return false;
            }

            value = cluster.data[localId];
            return cluster.hasEntity[localId];
        }

        public ref T GetOrInit(Entity entity) {
            int clusterId = entity.id / clusterSize_;
            int localId = entity.id % clusterSize_;

            // TODO: Investigate resizing strategy.
            if (clusterId >= allClusters.Length)
                Array.Resize(ref allClusters, clusterId + 1);

            Cluster cluster = allClusters[clusterId];
            if (cluster == null) {
                cluster = new Cluster(clusterSize_, entity.id - localId);
                clusterIndices.Add(clusterId);
                allClusters[clusterId] = cluster;
            }

            cluster.hasEntity[localId] = true;
            return ref cluster.data[localId];
        }

        public bool Has(Entity entity) {
            T dummy;
            return TryGet(entity, out dummy);
        }

        public void CopyTo(IEntityDictionary<T> dest_) {
            var dest = (SparseEntityDictionary<T>)dest_;

            if (dest.clusterSize_ != clusterSize_)
                throw new InvalidOperationException("Cluster size cannot different (too inefficient)");

            // TODO: This can probably be elided if the bags are the same length.
            dest.clusterIndices.CopyFrom(clusterIndices);

            if (allClusters.Length > dest.allClusters.Length)
                Array.Resize(ref dest.allClusters, allClusters.Length);

            foreach (int clusterIdx in clusterIndices) {
                if (dest.allClusters[clusterIdx] == null)
                    dest.allClusters[clusterIdx] = new Cluster(clusterSize_, allClusters[clusterIdx].entityStart);

                allClusters[clusterIdx].CopyTo(dest.allClusters[clusterIdx]);
            }
        }

        public IEnumerator<KeyValuePair<Entity, T>> GetEnumerator() => new Enumerator(this);
        IEnumerator IEnumerable.GetEnumerator() => new Enumerator(this);

        public class Cluster {
            // TODO: Consider maintaining a count of true values inside of
            //       hasEntity. That will let us bail out of iteration early
            //       once we have seen enough entities.

            public int entityStart;
            public bool[] hasEntity;
            public T[] data;

            public Cluster(int clusterSize, int entityStart) {
                this.entityStart = entityStart;
                hasEntity = new bool[clusterSize];
                data = new T[clusterSize];
            }

            public void CopyTo(Cluster dest) {
                if (entityStart != dest.entityStart)
                    throw new ArgumentException("entityStart should never get out of sync");

                // TODO: We could track min/max values inside of data so we copy a minimum number
                //       of objects if this is a perf problem.
                Array.Copy(hasEntity, dest.hasEntity, hasEntity.Length);
                Array.Copy(data, dest.data, data.Length);
            }
        }

        struct Enumerator : IEnumerator<KeyValuePair<Entity, T>> {
            private SparseEntityDictionary<T> storage_;

            private int clusterIndiciesIdx_;
            private Cluster cluster_;
            private int entityIdx_;

            public Enumerator(SparseEntityDictionary<T> storage) {
                storage_ = storage;
                clusterIndiciesIdx_ = -1;
                cluster_ = null;
                entityIdx_ = -1;
            }

            public KeyValuePair<Entity, T> Current {
                get {
                    return new KeyValuePair<Entity, T>(
                        new Entity(cluster_.entityStart + entityIdx_),
                        cluster_.data[entityIdx_]);
                }
            }
            object IEnumerator.Current => Current;

            public void Dispose() { }

            public bool MoveNext() {
                if (cluster_ == null && !MoveToNextCluster())
                    return false;

                while (!MoveToNextEntityInCluster()) {
                    if (!MoveToNextCluster())
                        return false;
                }

                return true;
            }

            private bool MoveToNextEntityInCluster() {
                ++entityIdx_;
                if (entityIdx_ >= cluster_.data.Length)
                    return false;

                while (!cluster_.hasEntity[entityIdx_]) {
                    entityIdx_++;
                    if (entityIdx_ >= cluster_.data.Length)
                        return false;
                }
                return true;

            }

            private bool MoveToNextCluster() {
                entityIdx_ = -1;
                ++clusterIndiciesIdx_;
                if (clusterIndiciesIdx_ >= storage_.clusterIndices.Length)
                    return false;
                cluster_ = storage_.allClusters[storage_.clusterIndices[clusterIndiciesIdx_]];
                return true;
            }

            public void Reset() {
                clusterIndiciesIdx_ = -1;
                cluster_ = null;
                entityIdx_ = -1;
            }
        }
    }
}