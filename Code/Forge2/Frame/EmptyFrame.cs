﻿using System;

namespace Forge2 {
    /// <summary>
    /// An EmptyFrame contains no data. It allows initialization objects
    /// for type |T| but does not actually receive them.
    /// <br />
    /// This is most useful for adding data instances that are merely tags.
    /// </summary>
    // TODO: Remove this class, not useful / actively harmful if we want to receive init data.
    public class EmptyFrame<T> : IFrame, IInitReceiver {
        void IFrame.OnFrameCompleted() { }
        Type IFrame.DataType => typeof(T);
        Type IInitReceiver.InitType => typeof(T);
    }
}
