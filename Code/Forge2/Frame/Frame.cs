﻿using System;

namespace Forge2 {
    // TODO: IFrame almost certainly needs to extend IEntityLifecycle.

    // A frame stores a set of data.
    public interface IFrame {
        void OnFrameCompleted();
        Type DataType { get; }
    }
}
