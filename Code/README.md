# Forge2

Forge2 is an extremely high-performance but ergonomic ECS framework. The framework provides abstracts only the minimum required to allow optimal game performance.

One of the key differences between Forge2 and other ECS frameworks is that Forge2 is not very opinionated. For example, there are a set of prebuilt component/data storage types but none of them need to be used.

## Concepts

### Entity

There are three primary concepts inside of the framework. An `Entity` is a simple numeric ID (`int`) that is used to associated different groups of data together. Each `Entity` acts as a unique key into the all of the different `Frame` instances.

- Every entity is created from a `Template` instance, without exception. A `Template` is simply a set of data instances that provide `Frames` with initial values. The individual initializers inside of the template can be overridden, but data types cannot be added or removed from the template.

### Frame

A `Frame` is simply a collection of data instances. Each `Frame` instance is free to store its data/component instances however it wishes. The framework ships with a number of common implementations.

For example, a `PositionFrame` could store positions in separate read/write arrays while also maintaining a quad tree to support optimized spatial queries, whereas a `NameFrame` will only use an `string[]` array where changes to the data are immediately visible to readers. Even more minimally, there could be a `TagFrame` which does not actually store any data - it just provides zero-overhead metadata to the framework which enables a `Filter` on arbitrary types.

### Filter

Forge2 provides `Filters` which contain a set of entities containing some combination of data instances.

- `Filter` instances cannot be created dynamically. When initializing the framework all filters must be given upfront.

These two limitations are critical for the extremely high `Filter` performance. When instantiating a `Template`, we know immediately which `Filter` instances need adjusting.

## Example: Print names

```c#
// A simple system that prints the last position of all Entities that have a name.
class PrintNames : System {
    [AutoRegister] Filter filter = Filter.AllOf<HealthData, NameData>();
    [AutoRegister] NameData.FrameStorage nameFrame = null;
    [AutoRegister] PositionData.FrameStorage positionFrame = null;

    public override void Update() {
        foreach (Entity entity in filter) {
            NameData name = nameFrame[entity];
            Vector3 position = positionFrame.readable[entity];
            Console.WriteLine($"{entity} {name} was at {position}");
        }
    }
}
```

## Example: Apply damage

Here is another example that implements a simple damage system.

```c#
public struct Damage {
    // TODO: This frame should not support initialization
    public class FrameStorage : UnorderedDenseFrame<Damage> { }

    public int amount;
    public Entity destination;
    public Entity source;
}

public class ApplyDamage : System {
    [AutoRegister]
    private Damage.FrameStorage damageFrame = null;
    [AutoRegister]
    private HealthData.FrameStorage healthFrame = null;

    // Fast processing of damage.
    public override void Update() {
        var wHealthFrame = healthFrame.writable;

        foreach (Damage damage in damageFrame.storage) {
            if (!wHealthFrame.storage.Has(damage.destination))
                throw new InvalidOperationException("Damage applied to entity with no health");
            wHealthFrame[damage.destination].health -= damage.amount;
        }

        damageFrame.storage.Clear();
    }

    // Example function to submit damage that will be processed later.
    void SubmitDamage(Entity source, Entity destination, int amount) {
        damageFrame.storage.Add(new Damage {
            amount = amount,
            destination = destination,
            source = source
        });
    }

}
```

That's it! You're free to implement systems however you wish. In the future, Forge2 may include a minimal framework that provides automatic system scheduling based on a dependency graph.


