﻿using System;
using System.Linq;
using UnityEngine;

namespace Game {
    public static class DebugTools {
        public static void DrawRect(Rect rect, float y, Color color) {
            var tr = new Vector3(rect.x0, y, rect.y0);
            var tl = new Vector3(rect.x1, y, rect.y0);
            var br = new Vector3(rect.x0, y, rect.y1);
            var bl = new Vector3(rect.x1, y, rect.y1);
            Debug.DrawLine(tr, tl, color);
            Debug.DrawLine(tl, bl, color);
            Debug.DrawLine(bl, br, color);
            Debug.DrawLine(br, tr, color);
        }

        public static void DrawRect(UnityEngine.Rect rect, float y, Color color) {
            var tr = new Vector3(rect.xMin, y, rect.yMin);
            var tl = new Vector3(rect.xMax, y, rect.yMin);
            var br = new Vector3(rect.xMin, y, rect.yMax);
            var bl = new Vector3(rect.xMax, y, rect.yMax);
            Debug.DrawLine(tr, tl, color);
            Debug.DrawLine(tl, bl, color);
            Debug.DrawLine(bl, br, color);
            Debug.DrawLine(br, tr, color);
        }
    }

    public class DebugQuad : MonoBehaviour {
        public bool drawTree = false;
        public UnityEngine.Rect queryArea;
        public int levels = 5;

        private void DrawNode<T>(QuadTree<T>.Node node, Color color) where T : IEquatable<T>, new() {
            DebugTools.DrawRect(node.area, node.level, color);
        }

        public void Update() {
            var tree = new QuadTree<int>(levels, new Dimension(10, 10));

            if (drawTree) {
                foreach (QuadTree<int>.Node[] level in tree.levelToNodes) {
                    foreach (QuadTree<int>.Node node in level) {
                        DrawNode(node, Color.red);
                    }
                }
            }

            DebugTools.DrawRect(queryArea, levels + 1, Color.green);
            foreach (QuadTree<int>.Node node in
                     tree.QueryNodes(Rect.FromUnityRect(queryArea))) {
                DrawNode(node, Color.blue);
            }
        }
    }
}
