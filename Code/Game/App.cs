﻿using System;
using Forge2;
using UnityEngine;
using UnityEngine.UI;
using UnityRandom = UnityEngine.Random;

namespace Game {

    public struct Damage {
        // TODO: This frame should not support initialization
        public class FrameStorage : UnorderedDenseFrame<Damage> { }

        public int amount;
        public Entity destination;
        public Entity source;
    }

    public class ApplyDamage : BaseSystem {
        [AutoRegister]
        private Damage.FrameStorage damageFrame = null;
        [AutoRegister]
        private HealthData.FrameStorage healthFrame = null;

        public override void Update() {
            var wHealthFrame = healthFrame.writable;

            foreach (Damage damage in damageFrame.storage) {
                if (!wHealthFrame.storage.Has(damage.destination))
                    throw new InvalidOperationException("Damage applied to entity with no health");
                wHealthFrame[damage.destination].health -= damage.amount;
            }

            damageFrame.storage.Clear();
        }
    }

    public struct DamageRandomEntity {
        public class FrameStorage : UnorderedDenseFrame<DamageRandomEntity, DamageRandomEntity> {
            protected override void Initialize(Entity entity, DamageRandomEntity initData, ref DamageRandomEntity frameData) {
                frameData = initData;
                frameData.owner = entity;
            }
        }

        public Entity owner;
        public int amount;
    }

    public class RunDamageRandomEntitySystem : BaseSystem {
        [AutoRegister]
        private Filter candidates = Filter.AllOf<HealthData>();
        [AutoRegister]
        private DamageRandomEntity.FrameStorage damageRandom = null;
        [AutoRegister]
        private Damage.FrameStorage postDamage = null;

        public override void Update() {
            if (candidates.entities.Length == 0)
                return;

            foreach (DamageRandomEntity data in damageRandom.storage) {
                // Find an entity to target that is *not* the originator.
                int destination;
                while (true) {
                    destination = UnityRandom.Range(0, candidates.entities.Length);
                    if (destination != data.owner.id)
                        break;
                }

                // Post damage to get processed later.
                postDamage.storage.Add(new Damage {
                    amount = data.amount,
                    source = data.owner,
                    destination = new Entity(destination)
                });
            }
        }

    }


    public struct UniqueId {
        public class FrameStorage : DenseFrame<UniqueId> {
            private int nextUniqueId = 0;

            protected override void Init(Entity entity, UniqueId initData, ref UniqueId frameData) {
                frameData.uniqueId = nextUniqueId++;
            }
        }

        public int uniqueId;

        public override string ToString() {
            return uniqueId.ToString();
        }
    }

    public struct NameData {
        public class FrameStorage : DenseFrame<NameData> { }

        public NameData(string name) {
            this.name = name;
        }

        public string name;

        public override string ToString() {
            return name;
        }
    }

    public struct HealthData {
        public class FrameStorage : SparseVersionedFrame<HealthData> { }

        public HealthData(int health) {
            this.health = health;
        }

        public int health;
    }
    public struct UnitData {
        public class FrameStorage : SparseVersionedFrame<UnitData> { }
    }
    public struct BuildingData {
        public class FrameStorage : SparseVersionedFrame<BuildingData> { }
    }

    public struct RegenData {
        public class FrameStorage : EmptyFrame<RegenData> { }
    }

    public class RegenBuildings : BaseSystem {
        [AutoRegister]
        public Filter toRegen = Filter
            .AllOf<HealthData, RegenData>()
            .AnyOf<UnitData, BuildingData>();
        [AutoRegister]
        private HealthData.FrameStorage healthFrame = null;
        /*
        [AutoRegister]
        private UnitData.FrameStorage unitFrame = null;
        [AutoRegister]
        private BuildingData.FrameStorage buildingFrame = null;
        */

        public override void Update() {
            foreach (Entity entity in toRegen) {
                ref HealthData health = ref healthFrame.writable[entity];
                health.health += 3;
            }
        }
    }

    public class App : MonoBehaviour {
        private Core core;

        private Template monster = Template.Build()
            .Add(new UniqueId())
            .Add(new NameData("Monster"))
            .Add(new PrefabData.Init("Monster"))
            .Add(new WanderData { speed = 1f })
            .Add(new Vector3())
            .Add(new HealthData(100))
            .Add(new DamageRandomEntity { amount = 1 });

        private Template civilian = Template.Build()
            .Add(new UniqueId())
            .Add(new NameData("Civilian"))
            .Add(new HealthData(10))
            .Add(new UnitData())
            .Add(new RegenData());

        private Template.Overrides john;

        public void Awake() {
            john = civilian.Override()
                .Add(new NameData("John"));

            var builder = new Core.Builder();

            builder.frames
                .Add<WanderData.Frame>()
                .Add<PrefabData.Frame>()
                .Add<Damage.FrameStorage>()
                .Add<DamageRandomEntity.FrameStorage>()
                .Add<UniqueId.FrameStorage>()
                .Add<NameData.FrameStorage>()
                .Add<Position.Frame>()
                .Add<HealthData.FrameStorage>()
                .Add<UnitData.FrameStorage>()
                .Add<BuildingData.FrameStorage>()
                .Add<RegenData.FrameStorage>();

            builder.systems
                .Add<WanderSystem>()
                .Add<SyncUnityPosition>()
                .Add<RunDamageRandomEntitySystem>()
                .Add<ApplyDamage>();
                //.Add<RegenBuildings>();

            core = builder.Build();
        }

        public bool shouldTick;
        public bool shouldPrint;

        public int monsterCount = 1;
        public int civilianCount = 25;
        public int johnCount = 1;

        public Text text;

        public void Update() {
            //Tick();

            if (shouldTick) {
                shouldTick = false;
                Tick();
            }

            if (shouldPrint) {
                shouldPrint = false;
                Print();
            }
        }

        public void Tick() {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            DoTick();
            stopwatch.Stop();
            double ms = stopwatch.ElapsedTicks / (float)System.Diagnostics.Stopwatch.Frequency * 1000.0;
            text.text = $"Update took {ms} ms with {core.entities.AliveCount} entities";
            Debug.Log(text.text);
        }

        private void DoTick() {
            for (int i = 0; i < monsterCount; ++i)
                core.entities.Create(monster);
            for (int i = 0; i < civilianCount; ++i)
                core.entities.Create(civilian);
            for (int i = 0; i < johnCount; ++i)
                core.entities.Create(john);

            core.Update();
        }

        public void Print() {
            foreach (var kvp in core.frames.GetFrame<HealthData.FrameStorage>().readable.storage) {
                Entity entity = kvp.Key;
                HealthData health = kvp.Value;
                NameData name = core.frames.GetFrame<NameData.FrameStorage>()[entity];
                Debug.Log($"{entity} {name} has {health.health} health");
            }
        }
    }
}
