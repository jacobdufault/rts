﻿using System.Collections.Generic;
using Forge2;
using UnityEngine;

namespace Game {

    public struct PathData {
        public class Frame : UnorderedDenseFrame<PathData> { }
    }

    public class PathSystem : BaseSystem {
        private struct Request {
            public List<Entity> entities;
            public Vector3 desintation;
        }
        private LinearAllocator<Request> requests_ = new LinearAllocator<Request>();

        public void RequestPath(List<Entity> entities, Vector3 destination) {
            requests_.Allocate() = new Request {
                entities = entities,
                desintation = destination
            };
        }

        public override void Update() {
            // TODO: I don't think we need to explicitly store a 'map'. Instead, we can just
            //       store a set of objects which block pathing/etc.

            foreach (Request request in requests_) {
            }

            requests_.Clear();
        }
    }
}
