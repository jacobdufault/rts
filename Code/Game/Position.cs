﻿using System;
using System.Collections.Generic;
using Forge2;
using UnityEngine;

namespace Game {
    public struct Position {
        public class Frame : IFrame, IInitReceiver<Vector3>, IEntityDestroyed {
            Type IFrame.DataType => typeof(Position);
            Type IInitReceiver.InitType => typeof(Vector3);

            public SparseEntityDictionary<int> entityToIndex = new SparseEntityDictionary<int>();
            public Bag<Hot> hot = new Bag<Hot>();
            public Bag<Cold> cold = new Bag<Cold>();

            public Bag<Entity> updatedPositions = new Bag<Entity>();

            private struct PositionUpdate {
                public Entity entity;
                public Vector3 position;
            }
            private LinearAllocator<PositionUpdate> positionUpdates_ = new LinearAllocator<PositionUpdate>();

            public void SetPosition(Entity entity, Vector3 newPosition) {
                positionUpdates_.Allocate() = new PositionUpdate {
                    entity = entity,
                    position = newPosition
                };
            }

            void IFrame.OnFrameCompleted() {
                updatedPositions.Clear();
                foreach (var update in positionUpdates_)
                    updatedPositions.Add(update.entity);

                // If this is too slow we could split it into more passes.
                foreach (var update in positionUpdates_) {
                    int idx = entityToIndex[update.entity];
                    hot[idx].previous = hot[idx].current;
                    hot[idx].current = update.position;
                }
                positionUpdates_.Clear();
            }

            void IInitReceiver<Vector3>.Init(Entity entity, Vector3 value) {
                hot.Add(new Hot {
                    current = value,
                    previous = value
                });
                cold.Add(new Cold {
                    entity = entity
                });
                entityToIndex.GetOrInit(entity) = hot.Length - 1;
            }

            private struct IndexUpdate {
                public Entity moved;
                public int newIndex;
            }

            void IEntityDestroyed.OnDestroyed(List<Entity> destroyed) {
                using (var scratch = new ScratchBuffer<IndexUpdate>(destroyed.Count)) {
                    foreach (Entity entity in destroyed) {
                        int index = entityToIndex[entity];
                        entityToIndex.Remove(entity);
                        hot.Remove(index);
                        cold.Remove(index);

                        // cold[index] could now point to a different entity. We need to
                        // update the reference inside of entityToIndex if that happened.
                        if (index < cold.Length) {
                            scratch.Add(new IndexUpdate {
                                moved = cold[index].entity,
                                newIndex = index
                            });
                        }
                    }

                    foreach (IndexUpdate update in scratch)
                        entityToIndex[update.moved] = update.newIndex;
                }
            }
        }

        public struct Hot {
            public Vector3 current;
            public Vector3 previous;
        }
        public struct Cold {
            public Transform transform;
            public Entity entity;
        }
    }

    public class SyncUnityPosition : BaseSystem {
        [AutoRegister]
        private Filter positions = Filter.AllOf<Position>();
        [AutoRegister]
        private Position.Frame positionFrame = null;
        [AutoRegister]
        private PrefabData.Frame prefabFrame = null;

        public override void Update() {
            // TODO: Do interpolation.

            foreach (Entity entity in positionFrame.updatedPositions) {
                int index = positionFrame.entityToIndex[entity];
                Transform transform = positionFrame.cold[index].transform;
                if (transform == null) {
                    transform = prefabFrame.storage[entity].instance.transform;
                    positionFrame.cold[index].transform = transform;
                }

                transform.position = positionFrame.hot[index].current;
            }
        }
    }
}
