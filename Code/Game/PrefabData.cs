﻿using System;
using System.Collections.Generic;
using Forge2;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace Game {
    public struct PrefabData {
        private class PrefabCache {
            private Dictionary<string, GameObject> cache_ = new Dictionary<string, GameObject>();

            public GameObject Load(string name) {
                GameObject result;
                if (!cache_.TryGetValue(name, out result)) {
                    result = Resources.Load<GameObject>(name);
                    if (result == null)
                        throw new InvalidOperationException($"Unable to load prefab with name {name}.");
                    cache_[name] = result;
                }

                return result;
            }
        }

        public class Frame : SparseFrame<PrefabData, Init> {
            private PrefabCache cache_ = new PrefabCache();

            protected override void Init(Entity entity, Init initData, ref PrefabData frameData) {
                GameObject prefab = cache_.Load(initData.path);
                GameObject instance = UnityObject.Instantiate(prefab);
                instance.name = $"Entity[{entity.id}] ({prefab.name})";
                frameData.instance = instance;
            }
        }

        public struct Init {
            public string path;

            public Init(string path) {
                this.path = path;
            }
        }

        public GameObject instance;
    }
}
