﻿namespace Game {
    public struct Vector2r {
        public Real x;
        public Real y;

        public Vector2r(Real x, Real y) {
            this.x = x;
            this.y = y;
        }

        public override string ToString() => $"[{x}, {y}]";
        public Real Length() => Real.Sqrt(x * x + y * y);

        public void Normalize() {
            Real length = Length();
            if (length == 0) {
                return;
            }

            x /= length;
            y /= length;
        }

        public static Real Distance(Vector2r a, Vector2r b) {
            return Distance(a.x, a.y, b.x, b.y);
        }

        public static Real Distance(Real x0, Real z0, Real x1, Real z1) {
            var dx = x0 - x1;
            var dz = z0 - z1;

            return Real.Sqrt(dx * dx + dz * dz);
        }

        public static Real DistanceSq(Vector2r a, Vector2r b) {
            return DistanceSq(a.x, a.y, b.x, b.y);
        }

        public static Real DistanceSq(Real x0, Real z0, Real x1, Real z1) {
            var dx = x0 - x1;
            var dz = z0 - z1;

            return dx * dx + dz * dz;
        }

        public static Vector2r operator -(Vector2r a, Vector2r b) {
            return new Vector2r() {
                x = a.x - b.x,
                y = a.y - b.y
            };
        }

        public static Vector2r operator +(Vector2r a, Vector2r b) {
            return new Vector2r() {
                x = a.x + b.x,
                y = a.y + b.y
            };
        }

        public static Vector2r operator *(Vector2r a, Real v) {
            return new Vector2r() {
                x = a.x * v,
                y = a.y * v
            };
        }

        public override bool Equals(object obj_) {
            if (obj_ is Vector2r == false)
                return false;
            var obj = (Vector2r)obj_;
            return x == obj.x && y == obj.y;
        }

        public bool Equals(Vector2r p) {
            return x == p.x && y == p.y;
        }

        public override int GetHashCode() {
            int hash = 5;
            hash *= (29 + x.GetHashCode());
            hash *= (29 + y.GetHashCode());
            return hash;
        }

        public static bool operator ==(Vector2r a, Vector2r b) {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(Vector2r a, Vector2r b) {
            return !(a == b);
        }
    }
}