﻿using System;
using System.Collections.Generic;
using Forge2;
using UnityEngine;

namespace Game.Old {
    /// <summary>
    /// Contains some utility functions that translate between two coordinate systems, called world
    /// and index. The world coordinate system is larger than the index coordinate system, ie, every
    /// 100 units in the world coordinate system maps to 1 unit in the index coordinate system.
    /// Further, the world coordinate system contains all 4 quadrants (+-x, +-z), but the index
    /// coordinate system only contains +x and +z.
    /// </summary>
    internal static class WorldIndexCoordinateTransform {
        /// <summary>
        /// Converts the given game coordinates (which can be negative) into coordinates on a
        /// smaller map with only positive coordinates.
        /// </summary>
        /// <param name="worldScale">The scale difference between the world map and the index map; a
        /// value of 100 here means that 100 units of the world map correspond to 1 unit the index
        /// map.</param>
        public static void ConvertWorldToIndex(int worldScale, int xWorld, int yWorld, out int xIndex, out int yIndex) {
            xIndex = MapWorldToIndex(worldScale, xWorld);
            yIndex = MapWorldToIndex(worldScale, yWorld);
        }

        /// <summary>
        /// Converts the given world coordinate into an index coordinate.
        /// </summary>
        /// <param name="worldScale">The scale difference between the world map and the index map; a
        /// value of 100 here means that 100 units of the world map correspond to 1 unit the index
        /// map.</param>
        /// <param name="world">The world coordinate (can be negative).</param>
        /// <returns>The index coordinate that is associated with the given world
        /// coordinate.</returns>
        public static int MapWorldToIndex(int worldScale, int world) {
            if (world >= 0)
                return (world / worldScale) * 2;
            return (-(world + 1) / worldScale) * 2 + 1;
        }

        /// <summary>
        /// Converts the given index coordinates into the most closely associated world coordinates.
        /// </summary>
        /// <param name="worldScale">The scale difference between the world map and the index map; a
        /// value of 100 here means that 100 units of the world map correspond to 1 unit the index
        /// map.</param>
        public static void ConvertIndexToWorld(int worldScale, int xIndex, int yIndex, out int xWorld, out int yWorld) {
            xWorld = MapIndexToWorld(worldScale, xIndex);
            yWorld = MapIndexToWorld(worldScale, yIndex);
        }

        public static int MapIndexToWorld(int worldScale, int index) {
            // positive world coordinate
            if (index % 2 == 0)
                return (index / 2) * worldScale;

            // negative world coordinate
            return (-(index + 1) / 2) * worldScale;
        }
    }

    public struct CircleBounds {
        public readonly Real x;
        public readonly Real y;
        public readonly Real radius;

        public CircleBounds(Real x, Real y, Real radius) {
            if (radius <= 0)
                throw new ArgumentException($"{nameof(radius)} must be > 0");

            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        public bool Intersects(CircleBounds other) {
            Real distanceBetweenCentersSq = Vector2r.DistanceSq(x, y, other.x, other.y);
            Real maxIntersectionDistanceSq = (radius + other.radius) * (radius + other.radius);
            return maxIntersectionDistanceSq >= distanceBetweenCentersSq;
        }

        public bool Contains(Vector2r point) => Contains(point.x, point.y);
        public bool Contains(Real x, Real z) {
            Real distanceSq = Vector2r.DistanceSq(this.x, y, x, z);
            return (radius * radius) > distanceSq;
        }

        public override string ToString() => $"{nameof(CircleBounds)} [center=({x}, {y}) radius={radius}]";
    }

    /// <summary>
    /// Interface for objects which are monitoring a specific region within a QuadTree.
    /// </summary>
    public interface IQuadTreeMonitor<T> {
        /// <summary>
        /// Called when the given item has entered the region.
        /// </summary>
        void OnEnter(T item);

        /// <summary>
        /// Called when an item has left the region.
        /// </summary>
        void OnExit(T item);
    }

    public struct Rect {
        public int x0;
        public int y0;
        public int x1;
        public int y1;

        public Rect(int x0, int y0, int x1, int y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }

        public Rect(CircleBounds bound) {
            x0 = (bound.x - bound.radius).AsInt;
            y0 = (bound.y - bound.radius).AsInt;
            x1 = (bound.x + bound.radius).AsInt;
            y1 = (bound.y + bound.radius).AsInt;
        }

        public static Rect Union(Rect a, Rect b) {
            int x0 = Math.Min(a.x0, b.x0);
            int z0 = Math.Min(a.y0, b.y0);
            int x1 = Math.Max(a.x1, b.x1);
            int z1 = Math.Max(a.y1, b.y1);

            return new Rect(x0, z0, x1, z1);
        }

        /// <summary>
        /// Returns true if this rect is either intersecting with or fully contains the parameter
        /// rect.
        /// </summary>
        public bool Colliding(Rect rect) {
            bool xCollides =
                (InRange(rect.x0, x0, x1)) ||
                (InRange(rect.x1, x0, x1));
            bool zCollides =
                (InRange(rect.y0, y0, y1)) ||
                (InRange(rect.y1, y0, y1));

            return xCollides && zCollides;
        }

        /// <summary>
        /// Returns true if v is between [min, max).
        /// </summary>
        private static bool InRange(int v, int min, int max) {
            return v >= min && v < max;
        }
    }

    // TODO: Kill generic type T, make this associate on Entity only.

    // TODO: the linear searches in this class can be removed by using metadata
    internal class Node<T> {
        public struct StoredMonitor {
            public IQuadTreeMonitor<T> monitor;
            // TODO: Make this IBounds since it doesn't *have* to be a circle?
            public CircleBounds region;

            public StoredMonitor(IQuadTreeMonitor<T> monitor, CircleBounds region) {
                this.monitor = monitor;
                this.region = region;
            }
        }

        public struct StoredItem {
            public T item;
            public Vector2r position;

            public StoredItem(T item, Vector2r position) {
                this.item = item;
                this.position = position;
            }
        }

        public Bag<StoredItem> items;
        public Bag<StoredMonitor> monitors;
        public Rect monitoredRegion;

        public Node(Rect monitoredRegion) {
            items = new Bag<StoredItem>();
            monitors = new Bag<StoredMonitor>();
            this.monitoredRegion = monitoredRegion;
        }

        public void Add(T item, Vector2r position) {
            items.Add(new StoredItem(item, position));

            // Dispatch OnEnter monitor calls.
            // TODO: If this monitor is monitoring two regions,
            //       it is possible it will get called twice.
            foreach (StoredMonitor monitor in monitors) {
                if (monitor.region.Contains(position))
                    monitor.monitor.OnEnter(item);
            }
        }

        public void ChangePosition(T item, Vector2r previous, Vector2r current) {
            // update the stored item
            for (int i = 0; i < items.Length; ++i) {
                if (ReferenceEquals(items[i].item, item)) {
                    items[i].position = current;
                    break;
                }
            }

            // update the monitors
            for (int i = 0; i < monitors.Length; ++i) {
                StoredMonitor monitor = monitors[i];

                bool containedPrevious = monitor.region.Contains(previous);
                bool containedCurrent = monitor.region.Contains(current);

                // the monitor was previously interested but no longer is
                if (containedPrevious && containedCurrent == false) {
                    monitor.monitor.OnExit(item);
                }

                // the monitor was not previous interested but now is
                else if (containedPrevious == false && containedCurrent) {
                    monitor.monitor.OnEnter(item);
                }
            }
        }

        public void Remove(T item, Vector2r position) {
            // remove the stored item
            for (int i = 0; i < items.Length; ++i) {
                if (ReferenceEquals(items[i].item, item)) {
                    items.Remove(i);
                    break;
                }
            }

            // call OnExit for all monitors that were previously interested in the item
            for (int i = 0; i < monitors.Length; ++i) {
                StoredMonitor monitor = monitors[i];
                if (monitor.region.Contains(position)) {
                    monitor.monitor.OnExit(item);
                }
            }
        }

        public void Add(IQuadTreeMonitor<T> monitor, CircleBounds monitoredRegion) {
            monitors.Add(new StoredMonitor(monitor, monitoredRegion));

            // check to see if the monitor is interested in any of our stored items
            for (int i = 0; i < items.Length; ++i) {
                StoredItem item = items[i];
                if (monitoredRegion.Contains(item.position)) {
                    monitor.OnEnter(item.item);
                }
            }
        }

        /// <summary>
        /// Adds all of the items inside of this node that are contained within the given region to
        /// the given collection.
        /// </summary>
        /// <param name="region">The area to collect objects from.</param>
        /// <param name="storage">Where to store the collected objects.</param>
        public void CollectInto(CircleBounds region, ICollection<T> storage) {
            for (int i = 0; i < items.Length; ++i) {
                StoredItem item = items[i];
                if (region.Contains(item.position)) {
                    storage.Add(item.item);
                }
            }
        }

        public void Update(IQuadTreeMonitor<T> monitor, CircleBounds previousRegion, CircleBounds currentRegion) {
            // update the stored monitor
            for (int i = 0; i < monitors.Length; ++i) {
                if (ReferenceEquals(monitors[i].monitor, monitor)) {
                    monitors[i].region = currentRegion;
                    break;
                }
            }

            // update the monitor
            for (int i = 0; i < items.Length; ++i) {
                StoredItem item = items[i];

                bool containedPrevious = previousRegion.Contains(item.position);
                bool containedCurrent = currentRegion.Contains(item.position);

                // the monitor was previously interested but no longer is
                if (containedPrevious && containedCurrent == false) {
                    monitor.OnExit(item.item);
                }

                // the monitor was not previous interested but now is
                else if (containedPrevious == false && containedCurrent) {
                    monitor.OnEnter(item.item);
                }
            }
        }

        public void Remove(IQuadTreeMonitor<T> monitor, CircleBounds monitoredRegion) {
            // remove the stored monitor
            for (int i = 0; i < monitors.Length; ++i) {
                if (ReferenceEquals(monitors[i].monitor, monitor)) {
                    monitors.Remove(i);
                    break;
                }
            }

            // remove all stored items from the monitor
            for (int i = 0; i < items.Length; ++i) {
                StoredItem item = items[i];

                if (monitoredRegion.Contains(item.position)) {
                    monitor.OnExit(item.item);
                }
            }
        }
    }

    /// <summary>
    /// Implements a QuadTree, which supports spatial monitoring and spatial querying of
    /// positionable objects. The objects can be positioned anywhere, even at negative coordinates.
    /// </summary>
    /// <remarks>
    /// In regards to implementation details, this is not currently a recursive QuadTree. Instead,
    /// the world is divided into a set of chunks which can be queried directly. The size of these
    /// chunks can be controlled by the constructor parameter.
    /// </remarks>
    /// <typeparam name="T">The type of object stored in the QuadTree</typeparam>
    public class QuadTree<TItem> {
        // The width/height of each chunk
        private int _worldScale;
        private Node<TItem>[,] _chunks;

        // Constructs an empty QuadTree. For optimal performance, |worldScale| should be close to
        // the typical query size.
        public QuadTree(int worldScale = 100) {
            _worldScale = worldScale;
            _chunks = new Node<TItem>[0, 0];
        }

        #region Query APIs
        /// <summary>
        /// Returns all monitors that are stored in the tree.
        /// </summary>
        public IEnumerable<IQuadTreeMonitor<TItem>> Monitors {
            get {
                var monitors = new HashSet<IQuadTreeMonitor<TItem>>();

                foreach (var chunk in _chunks) {
                    foreach (Node<TItem>.StoredMonitor monitor in chunk.monitors) {
                        monitors.Add(monitor.monitor);
                    }
                }

                return monitors;
            }
        }

        /// <summary>
        /// Returns all items that are stored in the tree.
        /// </summary>
        public IEnumerable<TItem> Items {
            get {
                var items = new HashSet<TItem>();

                foreach (var chunk in _chunks) {
                    foreach (Node<TItem>.StoredItem item in chunk.items) {
                        items.Add(item.item);
                    }
                }

                return items;
            }
        }
        #endregion

        #region Item API
        /// <summary>
        /// Add a new item to the QuadTree at the given position.
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <param name="position">The position of the item.</param>
        public void AddItem(TItem item, Vector2r position) {
            var chunk = GetChunk(position.x.AsInt, position.y.AsInt);
            chunk.Add(item, position);
        }

        /// <summary>
        /// Update the position of an item. This will notify monitors of position updates.
        /// </summary>
        /// <param name="item">The item to update the position of.</param>
        /// <param name="previous">The old position of the item.</param>
        /// <param name="current">The updated position of the item.</param>
        public void UpdateItem(TItem item, Vector2r previous, Vector2r current) {
            var previousChunk = GetChunk(previous.x.AsInt, previous.y.AsInt);
            var currentChunk = GetChunk(current.x.AsInt, current.y.AsInt);

            if (ReferenceEquals(previousChunk, currentChunk)) {
                previousChunk.ChangePosition(item, previous, current);
            }
            else {
                previousChunk.Remove(item, previous);
                currentChunk.Add(item, current);
            }
        }

        /// <summary>
        /// Remove an item from the QuadTree.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        /// <param name="position">The position of the item.</param>
        public void RemoveItem(TItem item, Vector2r position) {
            var chunk = GetChunk(position.x.AsInt, position.y.AsInt);
            chunk.Remove(item, position);
        }

        /// <summary>
        /// Collect all items that are stored inside of the given region.
        /// </summary>
        /// <param name="region">The area to collect entities from.</param>
        /// <param name="storage">Where to store the collected entities.</param>
        /// <typeparam name="TCollection">The type of collection to store items in.</typeparam>
        /// <returns>All entities that are contained in or intersecting with the given
        /// region.</returns>
        public TCollection CollectItems<TCollection>(CircleBounds region, TCollection storage = null)
            where TCollection : class, ICollection<TItem>, new() {

            if (storage == null) {
                storage = new TCollection();
            }

            IterateChunks(region, node => {
                node.CollectInto(region, storage);
            });

            return storage;
        }
        #endregion

        #region Monitor API
        /// <summary>
        /// Inserts the given monitor into the QuadTree. The monitor will be notified of any
        /// entities that enter or leave it. The monitor can be updated or removed.
        /// </summary>
        /// <param name="monitor">The monitor.</param>
        /// <param name="monitoredRegion">The area that the monitor is viewing.</param>
        public void AddMonitor(IQuadTreeMonitor<TItem> monitor, CircleBounds monitoredRegion) {
            IterateChunks(monitoredRegion, node => {
                node.Add(monitor, monitoredRegion);
            });
        }

        /// <summary>
        /// Removes the given monitor from the quad tree. It will receive a series of OnExit calls
        /// during this Remove call.
        /// </summary>
        /// <param name="monitor">The monitor to remove.</param>
        /// <param name="monitoredRegion">The region that the monitor was monitoring.</param>
        public void RemoveMonitor(IQuadTreeMonitor<TItem> monitor, CircleBounds monitoredRegion) {
            IterateChunks(monitoredRegion, node => {
                node.Remove(monitor, monitoredRegion);
            });
        }

        /// <summary>
        /// Update the position of a monitor.
        /// </summary>
        /// <param name="monitor">The monitor whose position has changed.</param>
        /// <param name="previousRegion">The previous area that the monitor was watching.</param>
        /// <param name="currentRegion">The new area that the monitor is watching.</param>
        public void UpdateMonitor(IQuadTreeMonitor<TItem> monitor, CircleBounds previousRegion, CircleBounds currentRegion) {
            // convert the bounds to rectangles
            var previousRect = new Rect(previousRegion);
            var currentRect = new Rect(currentRegion);

            var merged = Rect.Union(previousRect, currentRect);

            IterateChunks(merged, node => {
                bool previousContains = previousRect.Colliding(node.monitoredRegion);
                bool currentContains = currentRect.Colliding(node.monitoredRegion);

                // the monitor is no longer interested in this chunk
                if (previousContains && currentContains == false) {
                    node.Remove(monitor, previousRegion);
                }

                // the monitor is still interested in this chunk
                else if (previousContains && currentContains) {
                    node.Update(monitor, previousRegion, currentRegion);
                }

                // the monitor is now interested in this chunk, but was not before
                else if (previousContains == false && currentContains) {
                    node.Add(monitor, currentRegion);
                }
            });
        }
        #endregion

        #region Chunk Management
        /// <summary>
        /// Helper function to iterate all of the chunks that are contained within the given world
        /// region.
        /// </summary>
        private void IterateChunks(CircleBounds region, Action<Node<TItem>> onChunk) {
            IterateChunks(new Rect(region), onChunk);
        }

        /// <summary>
        /// Helper function to iterate all of the chunks that are contained within the given world
        /// region.
        /// </summary>
        private void IterateChunks(Rect region, Action<Node<TItem>> onChunk) {
            for (int x = region.x0; x < (region.x1 + _worldScale); x += _worldScale) {
                for (int z = region.y0; z < (region.y1 + _worldScale); z += _worldScale) {
                    onChunk(GetChunk(x, z));
                }
            }
        }

        /// <summary>
        /// Returns the chunk located at the given x and z world coordinates.
        /// </summary>
        private Node<TItem> GetChunk(int xWorld, int zWorld) {
            int xIndex, zIndex;
            WorldIndexCoordinateTransform.ConvertWorldToIndex(_worldScale,
                xWorld, zWorld, out xIndex, out zIndex);

            if (xIndex >= _chunks.GetLength(0) || zIndex >= _chunks.GetLength(1)) {
                _chunks = GrowArray(_chunks, xIndex + 1, zIndex + 1, GenerateChunk);
            }

            return _chunks[xIndex, zIndex];
        }

        /// <summary>
        /// Creates a new chunk located at the given x and z index coordinates.
        /// </summary>
        private Node<TItem> GenerateChunk(int xIndex, int zIndex) {
            int xWorld, zWorld;
            WorldIndexCoordinateTransform.ConvertIndexToWorld(_worldScale,
                xIndex, zIndex, out xWorld, out zWorld);

            var rect = new Rect(xWorld, zWorld, xWorld + _worldScale, zWorld + _worldScale);
            return new Node<TItem>(rect);
        }

        /// <summary>
        /// Helper method that grows a 2D array and populates new elements with values created from
        /// the generator.
        /// </summary>
        private static T[,] GrowArray<T>(T[,] original, int newLengthX, int newLengthZ, Func<int, int, T> generator) {
            var newArray = new T[newLengthX, newLengthZ];

            int originalLengthX = original.GetLength(0);
            int originalLengthZ = original.GetLength(1);

            for (int x = 0; x < newLengthX; x++) {
                for (int z = 0; z < newLengthZ; z++) {
                    T stored;
                    if (x >= originalLengthX || z >= originalLengthZ) {
                        stored = generator(x, z);
                    }
                    else {
                        stored = original[x, z];
                    }

                    newArray[x, z] = stored;
                }
            }

            return newArray;
        }
        #endregion
    }
}