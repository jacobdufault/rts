﻿#if false
    public struct Rect {
        public int x0;
        public int y0;
        public int x1;
        public int y1;

        public int left => x0;
        public int top => y0;
        public int right => x1;
        public int bottom => y1;
    }

    // TODO: Make struct.
    public class QuadNode {
        public int x1, y1, x2, y2;
        public bool isLeafNode;
        public QuadNode ULNode, URNode, LLNode, LRNode;

        public QuadNode GetNodeContaining(Rect ObjBounds) {
            if (!isLeafNode) {
                if (ULNode.Contains(ObjBounds))
                    return ULNode.GetNodeContaining(ObjBounds);
                if (URNode.Contains(ObjBounds))
                    return URNode.GetNodeContaining(ObjBounds);
                if (LLNode.Contains(ObjBounds))
                    return LLNode.GetNodeContaining(ObjBounds);
                if (LRNode.Contains(ObjBounds))
                    return LRNode.GetNodeContaining(ObjBounds);
            }

            return this;
        }

        public bool Contains(Rect ObjBounds) {
            return (ObjBounds.top >= y1 && ObjBounds.left >= x1 &&
                    ObjBounds.bottom <= y2 && ObjBounds.right <= x2);
        }
    }

    public class QuadTree {
        public QuadNode RootNode;
        QuadNode GetNodeContainingSlow(Rect ObjBounds) {
            if (RootNode.Contains(ObjBounds))
                return RootNode.GetNodeContaining(ObjBounds);
            else
                return null;
        }


        public int QuadXScale;
        public int QuadYScale;
        public int NumberOfTreeLevels;
        QuadNode[][] NodeLevelPointerArray;

        // Returns the quadtree level that the rect belongs to.
        public int GetNodeLevelContaining(Rect ObjBounds) {
            // TODO: int => uint?
            int xResult = ((int)(ObjBounds.left * QuadXScale)) ^
                          ((int)(ObjBounds.right * QuadXScale));
            int yResult = ((int)(ObjBounds.top * QuadYScale)) ^
                          ((int)(ObjBounds.bottom * QuadYScale));

            // Count highest bit position
            int NodeLevel = NumberOfTreeLevels;
            while (xResult + yResult != 0) {
                xResult >>= 1;
                yResult >>= 1;
                NodeLevel--;
            }

            return NodeLevel;
        }

        // Returns the specific node that contains the rect.
        public QuadNode GetNodeContaining(Rect ObjBounds) {
            int x1 = (int)(ObjBounds.left * QuadXScale);
            int y1 = (int)(ObjBounds.top * QuadYScale);

            int xResult = x1 ^ ((int)(ObjBounds.right * QuadXScale));
            int yResult = y1 ^ ((int)(ObjBounds.bottom * QuadYScale));

            int nodeLevel = NumberOfTreeLevels;
            int shiftCount = 0;

            // Count highest bit position
            while (xResult + yResult != 0) {
                xResult >>= 1;
                yResult >>= 1;
                nodeLevel--;
                shiftCount++;
            }

            // Now lookup the node pointer in a 2D array stored linearly
            // Scale coordinates for quadtree level
            x1 >>= shiftCount;
            y1 >>= shiftCount;

            QuadNode[] nodes = NodeLevelPointerArray[nodeLevel];
            return (nodes[y1 << (nodeLevel - 1) + x1]);
        }

    }

#endif

#if false























    // Original source code below:

    // Two C++ classes, one to represent the QuadTree and one to
    // Represent each individual node.
    // all variables are class members unless defined
    QuadNode* QuadTree::GetNodeContaining(const rect& ObjBounds) {
        if (RootNode->Contains(ObjBounds) )
            return RootNode->GetNodeContaining(ObjBounds);
        else
            return null;
    }

    QuadNode* QuadNode::GetNodeContaining(const rect& ObjBounds) {
        if (lisLeafNode) {
            if (ULNode->Contains(ObjBounds)
                return ULNode->GetNodeContaining(ObjBound);
            if (URNode->Contains(ObjBounds)
                return URNode->GetNodeContaining(ObjBound);
            if (LLNode->Contains(ObjBounds)
                return LLNode->GetNodeContaining(ObjBound);
            if (LRNode->Contains(ObjBounds)
                return LRNode->GetNodeContaining(ObjBound);
        }
        
        return this;
    }

    bool QuadNode::Contains(const rect& 0bjBounds) {
        return (ObjBounds.top >= y1 && ObjBounds.left >= x1 &&
                ObjBounds.bottom <= y2 && ObjBounds.right <= x2);
    }

    // Returns the quadtree level that the rect belongs to.
    int QuadTree::GetNodeLevelContaining(const rect& ObjBounds) {
        int xResult = ((int) (ObjBounds.left * QuadXScale)) ^
                      ((int) (ObjBounds.right * QuadXScale));
        int yResult = ((int) (ObjBounds.top * QuadYScale)) ^
                      ((int) (ObjBounds.bottom * QuadYScale));

        // Count highest bit position
        int NodeLevel = NumberOfTreeLevels;
        while (xResult + yResult != 0 ) {
            xResult »= 1;
            yResult »= 1;
            NodeLevel--;
        }

        return NodeLevel;
    }
    // Returns the specific node that contains the rect.
    QuadNode* QuadTree::GetNodeContaining(const rect &0bjBounds)
        int x1 = (int)(ObjBounds.left * QuadXScale);
        int y1 = (int)(ObjBounds.top * QuadYScale);

        int xResult = x1 ^ ((int) (ObjBounds.right * QuadXScale));
        int yResult = y1 ^ ((int) (ObjBounds.bottom * QuadYScale));

        int NodeLevel = NumberOfTreeLevels;
        int shiftCount = 0;

        // Count highest bit position
        while (xResult + yResult != 0 ) {
            xResult >>= 1;
            yResult >>= 1;
            NodeLevel--;
            ShiftCount++;
        }

        // Now lookup the node pointer in a 2D array stored linearly
        // Scale coordinates for quadtree level
        x1 >>= shiftCount; 
        y1 >>= shiftCount;

        QuadNode** nodes = NodeLevelPointerArray[NodeLevel];
        return (nodes[y1 << (NodeLevel - 1) + x1]);
    }

#endif