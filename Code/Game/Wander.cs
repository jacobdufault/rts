﻿using Forge2;
using UnityEngine;

namespace Game {
    public struct WanderData {
        public class Frame : SparseFrame<WanderData> { }

        public float speed;
    }

    public class WanderSystem : BaseSystem {
        [AutoRegister]
        private Filter wanderable = Filter.AllOf<Position, WanderData>();
        [AutoRegister]
        private WanderData.Frame wanderFrame = null;
        [AutoRegister]
        private Position.Frame positionFrame = null;

        public override void Update() {
            foreach (Entity entity in wanderable) {
                float speed = wanderFrame[entity].speed;

                Vector3 currentPosition = positionFrame.hot[positionFrame.entityToIndex[entity]].current;
                Vector3 delta = UnityEngine.Random.insideUnitCircle * speed;
                delta.z = delta.y;
                delta.y = 0;
                Vector3 dest = currentPosition + delta;

                positionFrame.SetPosition(entity, dest);
            }
        }
    }
}
