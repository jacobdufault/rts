﻿namespace Game {
    public struct Dimension {
        public int w;
        public int h;

        public Dimension(int w, int h) {
            this.w = w;
            this.h = h;
        }
    }
}
