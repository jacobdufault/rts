﻿namespace Game {
    public struct Rect {
        public int x0;
        public int y0;
        public int x1;
        public int y1;

        public int left => x0;
        public int top => y0;
        public int right => x1;
        public int bottom => y1;

        public int width => x1 - x0;
        public int height => y1 - y0;

        public static Rect FromWH(int w, int h) {
            return new Rect(0, 0, w, h);
        }
        public static Rect FromXYWH(int x, int y, int w, int h) {
            return new Rect(x, y, x + w, y + h);
        }
        public static Rect FromUnityRect(UnityEngine.Rect rect) {
            return FromXYWH((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
        }

        public Rect(int x0, int y0, int x1, int y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }

        // Returns true if this Rect fuly encloses or contains |other|.
        public bool Contains(Rect other) {
            return top <= other.top &&
                   left <= other.left &&
                   bottom >= other.bottom &&
                   right >= other.right;
        }

        public bool Intersects(Rect other) {
            return (other.x1 >= x0 && other.x0 <= x1) && (other.y1 >= y0 && other.y0 <= y1);
        }

        public override string ToString() => $"(x={x0}, y={y0}, w={width}, h={height})";
    }
}
