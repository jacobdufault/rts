﻿#define ENABLE_LOGS

using System;
using System.Collections.Generic;
using System.Linq;
using Forge2;

namespace Game {
    /// <summary>
    /// A cache-efficient QuadTree with extremely fast insert/removals/updates. Querying is
    /// relatively fast as well.
    /// </summary>
    /// <remarks>
    /// This is an implementation of "Direct Access Quadtree Lookup" from Game Programming Gems 2.
    /// </remarks>
    public class QuadTree<TData> where TData : IEquatable<TData>, new() {
        // TODO: Store inverted smallestNodeSize so we multiply instead of divide.
        // TODO: If we maintain the maximum size of a stored entry, then we running a
        //       query we do not have to scan every node upwards. We only need to expand
        //       the query size by the maximum size on each direction when fetching the
        //       initial node. If we have the maximum size we know at most how far away
        //       a node can be before entering the query region.
        // TODO: Implement persistent queries, ie,
        //      public struct PersistentQuery {
        //          public readonly Rect queryArea;
        //          // Query owner is responsible for clearing entered/left.
        //          public Bag<Entity> entered;
        //          public Bag<Entity> left;
        //      }

        /// <summary>
        /// Returns a QuadTree instance that is at least |desiredSize| in size.
        /// </summary>
        public static QuadTree<TData> CreateForDimensions(Dimension desiredSize, Dimension smallestNodeSize) {
            // It would probably be pretty easy to compute the value directly, but this is simple and
            // future proof.
            int numLevels = 1;
            while ((NumNodesPerAxis(numLevels) * smallestNodeSize.w) < desiredSize.w ||
                   (NumNodesPerAxis(numLevels) * smallestNodeSize.h) < desiredSize.h) {
                ++numLevels;
            }

            return new QuadTree<TData>(numLevels, smallestNodeSize);
        }

        public struct Item {
            public Item(Rect rect, TData data) {
                this.rect = rect;
                this.data = data;
            }

            public Rect rect;
            public TData data;
        }

        public struct Node {
            // Area that the node stores entries. Not really needed, but useful for debugging.
            public Rect area;
            // Level of the node in the tree. Not really needed, but useful for debugging.
            public int level;
            // Items stored in the node.
            public Bag<Item> items;

            public override string ToString() => $"Node [area={area}, items={items.Length}]";
        }

        // Inverted size of the smallest node (inverted means 1/x).
        private float invSmallestNodeSizeW, invSmallestNodeSizeH;

        // Size of the entire tree.
        public Rect bounds => levelToNodes[0][0].area;

        // Each level has 4 times as many elements as the level above it. Level 0 has 1 node.
        public Node[][] levelToNodes;

        public Dimension maxStoredSize;

        public QuadTree(int totalLevels, Dimension smallestNodeSize) {
            if (totalLevels <= 0)
                throw new ArgumentException("Need at least one level");
            if (totalLevels > 12)
                throw new ArgumentException("Too much memory, use a different QuadTree implementation or increase size of the smallest node");

            // TODO: Make this configurable.
            maxStoredSize = new Dimension(smallestNodeSize.w * 2, smallestNodeSize.h * 2);

            invSmallestNodeSizeW = 1.0f / smallestNodeSize.w;
            invSmallestNodeSizeH = 1.0f / smallestNodeSize.h;

            levelToNodes = new Node[totalLevels][];
            for (int level = 0; level < totalLevels; ++level) {
                int numNodesPerAxis = NumNodesPerAxis(level);
                int normalizedNodeLength = NormalizedNodeLength(level, totalLevels);

                levelToNodes[level] = new Node[numNodesPerAxis * numNodesPerAxis];
                for (int x = 0; x < numNodesPerAxis; ++x) {
                    for (int y = 0; y < numNodesPerAxis; ++y) {
                        int index = GetArrayIndex(x, y, level);
                        ref Node node = ref levelToNodes[level][index];
                        node.area = Rect.FromXYWH(
                            x * normalizedNodeLength * smallestNodeSize.w,
                            y * normalizedNodeLength * smallestNodeSize.h,
                            (normalizedNodeLength * smallestNodeSize.w) - 1,
                            (normalizedNodeLength * smallestNodeSize.h) - 1);
                        node.level = level;
                        node.items = new Bag<Item>();
                    }
                }
            }
        }

        public void Add(Rect area, TData item) {
            GetNodeContaining(area).items.Add(new Item(area, item));
        }

        public void Remove(Rect area, TData data) {
            // TODO: Make sure this doesn't allocate. Add equality comparision logic. Potentially
            //       store bag index inside of Item to make the removal O(1) (InvasiveBag?)
            GetNodeContaining(area).items.Remove(new Item(area, data));
        }

        public void Update(Rect previousArea, Rect newArea, TData data) {
            // Don't bother changing nodes if we did not update the location.
            GetSmallestNodeContaining(previousArea, out int prevLevel, out int prevX, out int prevY);
            GetSmallestNodeContaining(newArea, out int newLevel, out int newX, out int newY);
            if (prevLevel == newLevel && prevX == newX && prevY == newY)
                return;

            ref Node previous = ref levelToNodes[prevLevel][GetArrayIndex(prevX, prevY, prevLevel)];
            ref Node current = ref levelToNodes[newLevel][GetArrayIndex(newX, newY, newLevel)];
            previous.items.Remove(new Item(previousArea, data));
            current.items.Add(new Item(newArea, data));
        }

        private Rect RemapCoordinates(Rect normalizedArea, int level) {
            int nodeLength = NormalizedNodeLength(level, levelToNodes.Length);
            return new Rect(
                Math.Max((normalizedArea.x0 / nodeLength) - 1, 0),
                Math.Max((normalizedArea.y0 / nodeLength) - 1, 0),
                Math.Min((normalizedArea.x1 / nodeLength) + 1, NumNodesPerAxis(level) - 1),
                Math.Min((normalizedArea.y1 / nodeLength) + 1, NumNodesPerAxis(level) - 1));
        }

        // Return all items which are contained in or intersect with |queryArea|.
        public List<TData> Query(Rect queryArea) {
            var result = new List<TData>();

            // TODO: Compute initial capacity based on queryArea level.
            using (var nodes = ScratchBuffer.Get<Node>(200)) {
                QueryNodes(queryArea, nodes.buffer);
                foreach (Node node in nodes) {
                    foreach (Item item in node.items) {
                        // TODO: QueryNodes could return a partially occupied and a fully occupied
                        //       set, so that we can elide the |Intersects| call in some cases.
                        if (queryArea.Contains(item.rect) || queryArea.Intersects(item.rect)) {
                            result.Add(item.data);
                        }
                    }
                }
            }

            return result;
        }

        // TODO: Remove this method.
        public List<Node> QueryNodes(Rect queryArea) {
            var storage = new List<Node>();
            QueryNodes(queryArea, storage);
            return storage;
        }

        private Rect? GetExpandedQueryArea(Rect queryArea) {
            //return null;

            // TODO: Implement some logic here, we might not always want to use this approach.
            queryArea.x0 -= maxStoredSize.w;
            queryArea.x1 += maxStoredSize.w;
            queryArea.y0 -= maxStoredSize.h;
            queryArea.y1 += maxStoredSize.h;

            if (queryArea.x0 < 0)
                queryArea.x0 = 0;
            if (queryArea.y0 < 0)
                queryArea.y0 = 0;
            if (queryArea.x1 >= bounds.x1)
                queryArea.x1 = bounds.x1 - 1;
            if (queryArea.y1 >= bounds.y1)
                queryArea.y1 = bounds.y1 - 1;

            return queryArea;
        }

        // TODO: Make this private.
        public void QueryNodes(Rect queryArea, List<Node> storage) {
            // Querying is a little bit trickier than inserting and is not as fast. This
            // is because the a Node containing queryArea could contain an entry which
            // intersects queryArea. Note that the item in the parent Node will *not*
            // be fully contained in queryArea - otherwise, it would be in the same node
            // as queryArea.
            //
            // TODO: It may be possible to reduce overhead of querying parents/children by
            // introducing a maximum stored object size.
            //
            // We need to find:
            //   - Any node which contains or intersects with queryArea
            //   - Any node which queryArea contains
            //
            // We do an immediate index to the first Node fully enclosing queryArea.
            //  - Add that Node.
            //  - Scan up levels adding any which fully enclose queryArea.
            //  - Scan down adding any areas which queryArea encloses or intersects.

            // Expand queryArea by maximum stored node size. Nothing outside of that
            // expansion can intersect queryArea.
            Rect? expandedQueryArea = GetExpandedQueryArea(queryArea);

            GetSmallestNodeContaining(expandedQueryArea.HasValue ? expandedQueryArea.Value : queryArea,
                                      out int rootLevel, out int rootX, out int rootY);

            Rect normalizedArea = new Rect(
                (int)(queryArea.x0 * invSmallestNodeSizeW),
                (int)(queryArea.y0 * invSmallestNodeSizeH),
                (int)(queryArea.x1 * invSmallestNodeSizeW),
                (int)(queryArea.y1 * invSmallestNodeSizeH));

#if ENABLE_LOGS
            int processedCount = 0;
#endif

            if (!expandedQueryArea.HasValue) {
                // Add any areas which contain |queryArea|.
                for (int level = 0; level < rootLevel; ++level) {
                    Rect derivedIndices = RemapCoordinates(normalizedArea, level);
#if ENABLE_LOGS
                Console.WriteLine($"& L{level}: Derived Indices=[{derivedIndices}], Dimensions=({NumNodesPerAxis(level)},{NumNodesPerAxis(level)})");
#endif
                    Node[] nodes = levelToNodes[level];
                    for (int x = derivedIndices.x0; x <= derivedIndices.x1; ++x) {
                        for (int y = derivedIndices.y0; y <= derivedIndices.y1; ++y) {
#if ENABLE_LOGS
                        ++processedCount;
#endif
                            Node node = nodes[GetArrayIndex(x, y, level)];
                            if (node.area.Contains(queryArea))
                                storage.Add(node);
                        }
                    }
                }
            }

            // Add |rootLevel|. We don't need to loop since this is the only rect at this level
            // which can contain the rect.
            storage.Add(levelToNodes[rootLevel][GetArrayIndex(rootX, rootY, rootLevel)]);
#if ENABLE_LOGS
            ++processedCount;
#endif

            // Add any areas that |queryArea| contains or intersects with.
            for (int level = rootLevel + 1; level < levelToNodes.Length; ++level) {
                Rect derivedIndices = RemapCoordinates(normalizedArea, level);
#if ENABLE_LOGS
                Console.WriteLine($"| L{level}: Derived Indices=[{derivedIndices}], Dimensions=({NumNodesPerAxis(level)},{NumNodesPerAxis(level)})");
#endif
                Node[] nodes = levelToNodes[level];
                for (int x = derivedIndices.x0; x <= derivedIndices.x1; ++x) {
                    for (int y = derivedIndices.y0; y <= derivedIndices.y1; ++y) {
#if ENABLE_LOGS
                        ++processedCount;
#endif
                        Node node = nodes[GetArrayIndex(x, y, level)];
                        if (queryArea.Contains(node.area) || queryArea.Intersects(node.area))
                            storage.Add(node);
                    }
                }
            }

#if ENABLE_LOGS
            Console.WriteLine($"| Processed {processedCount}/{levelToNodes.Sum(array => array.Length)} nodes");
#endif
        }

        private void GetSmallestNodeContaining(Rect objBounds, out int level, out int x, out int y) {
            x = (int)(objBounds.left * invSmallestNodeSizeW);
            y = (int)(objBounds.top * invSmallestNodeSizeH);

            int xResult = x ^ ((int)(objBounds.right * invSmallestNodeSizeW));
            int yResult = y ^ ((int)(objBounds.bottom * invSmallestNodeSizeH));

            level = levelToNodes.Length - 1;
            int shiftCount = 0;

            // Count highest bit position
            while (xResult + yResult != 0) {
                xResult >>= 1;
                yResult >>= 1;
                level--;
                shiftCount++;
            }

            // Now lookup the node pointer in a 2D array stored linearly
            // Scale coordinates for quadtree level
            x >>= shiftCount;
            y >>= shiftCount;
        }

        // Returns the specific node that contains the rect.
        public ref Node GetNodeContaining(Rect area) {
            if (!bounds.Contains(area))
                throw new ArgumentException($"{area} is not contained in QuadTree bounds {bounds}");
            // TODO: should this be > or >=?
            if (area.width > maxStoredSize.w)
                throw new ArgumentException($"maximum stored width is {maxStoredSize.w}, but requested width is {area.width}");
            if (area.height > maxStoredSize.h)
                throw new ArgumentException($"maximum stored height is {maxStoredSize.h}, but requested width is {area.height}");

            GetSmallestNodeContaining(area, out int level, out int x, out int y);
            Node[] nodes = levelToNodes[level];
            return ref nodes[GetArrayIndex(x, y, level)];
        }

        // The length of a single node on the given level, ie, how long is it. For example,
        // L0 doubles for every additional level added. This is always 1 at the bottom level,
        // ie, totalLevels=8, level=7.
        private static int NormalizedNodeLength(int level, int totalLevels) {
            // Equivalent to 
            //  (int)Math.Pow(2, totalLevels - level - 1);
            return 1 << (totalLevels - level - 1);
        }

        // Returns the number of nodes per level. L0 always has 1 node, L1 always has 2 nodes,
        // L2 always has 4 nodes, etc.
        private static int NumNodesPerAxis(int level) {
            // Equivalent to 
            //  (int)Math.Pow(2, level)
            return 1 << level;
        }

        private static int GetArrayIndex(int x, int y, int level) {
            // Equivalent to
            //   (x * NumNodesPerAxis(level)) + y
            return x * (1 << level) + y;
        }
    }
}
