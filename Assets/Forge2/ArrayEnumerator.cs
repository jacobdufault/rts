using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2 {
    public struct Element<T> {
        private T[] array_;
        private int index_;

        public Element(T[] array, int index) {
            array_ = array;
            index_ = index;
        }

        public ref T value => ref array_[index_];
    }

    public class MyBag<T> : IEnumerable<Element<T>> {
        IEnumerator IEnumerable.GetEnumerator() {
            throw new NotImplementedException();
        }

        IEnumerator<Element<T>> IEnumerable<Element<T>>.GetEnumerator() {
            throw new NotImplementedException();
        }

        ArrayEnumerator<Element<T>> GetEnumerator() {
            throw new NotImplementedException();
        }
    }

    public class Test {
        public void Foo() {
            var bag = new MyBag<int>();
            foreach (Element<int> e in bag) {
                ref int val = ref e.value;
            }
        }
    }

    public struct ElementArrayEnumerator<T> : IEnumerator<Element<T>> {
        private T[] array_;
        private int length_;
        public int index_;

        public ElementArrayEnumerator(T[] array, int length) {
            array_ = array;
            length_ = length;
            index_ = -1;
        }

        object IEnumerator.Current => Current;
        public Element<T> Current => new Element<T>(array_, index_);

        public bool MoveNext() {
            index_ += 1;
            return index_ < length_;
        }

        public void Reset() {
            index_ = -1;
        }

        void IDisposable.Dispose() {
            array_ = null;
            length_ = 0;
            index_ = -1;
        }
    }

    public struct ArrayEnumerator<T> : IEnumerator<T> {
        private T[] array_;
        private int length_;
        public int index_;

        public ArrayEnumerator(T[] array, int length) {
            array_ = array;
            length_ = length;
            index_ = -1;
        }

        object IEnumerator.Current => Current;
        public T Current => array_[index_];

        public bool MoveNext() {
            index_ += 1;
            return index_ < length_;
        }

        public void Reset() {
            index_ = -1;
        }

        void IDisposable.Dispose() {
            array_ = null;
            length_ = 0;
            index_ = -1;
        }
    }
}