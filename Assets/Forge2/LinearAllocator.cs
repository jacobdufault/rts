﻿using System;

namespace Forge2 {
    public class LinearAllocator<T> {
        private T[] data_;
        private int length_;

        public LinearAllocator(int capacity = 64) {
            data_ = new T[capacity];
            length_ = 0;
        }

        public ref T Allocate() {
            length_ += 1;
            if (length_ > data_.Length)
                Array.Resize(ref data_, data_.Length * 2);

            return ref data_[length_ - 1];
        }

        public void Allocate(T value) {
            Allocate() = value;
        }

        public void Clear() {
            length_ = 0;
        }

        public ref T this[int idx] => ref data_[idx];
        public int Length => length_;

        public ArrayEnumerator<T> GetEnumerator() => new ArrayEnumerator<T>(data_, length_);
    }
}
