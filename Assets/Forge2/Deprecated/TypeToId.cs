﻿using System;
using System.Collections.Generic;

namespace Forge2.Old {
    // Abuse .NET statics to create a fast type => int mapping.
    public struct TypeToId {
        private static int numValues = 0;
        private static Dictionary<Type, int> typeToId_ = new Dictionary<Type, int>();

        public static Type GetTypeForId(int id) {
            foreach (var entry in typeToId_) {
                if (entry.Value == id)
                    return entry.Key;
            }

            throw new InvalidOperationException($"No type registered for id {id}");
        }

        public static int GetId(Type type) {
            int result;
            if (!typeToId_.TryGetValue(type, out result)) {
                result = numValues++;
                typeToId_[type] = result;
            }
            return result;
        }
        public static int GetId<T>() {
            return StaticStorage<T>.Value;
        }

        public struct StaticStorage<T> {
            static StaticStorage() {
                lock (typeof(TypeToId)) {
                    // Note: We must call the non-generic version, as that will initialize
                    //       the type in the typeToId_ dictionary.
                    Value = GetId(typeof(T));
                }
            }

            public static int Value;
        }
    }
}
