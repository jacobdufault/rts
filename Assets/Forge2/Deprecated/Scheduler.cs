﻿namespace Forge2.Scheduler {
    // Experimental scheduler API. This plans the execution order between
    // different frames.
    //
    // We build a forest of updatable frames by examining the frame
    // dependencies. Each forest can be executed entirely independently.
    //
    // Within a single forest, the graph should be a tree or something like
    // it. If there are no cycles, then we can execute the given nodes in
    // parallel, so long as their parents have already executed.

    public class Scheduler {
    }
}
