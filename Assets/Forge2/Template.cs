using System;
using System.Collections.Generic;
using System.Linq;

namespace Forge2 {
    public interface IInitReceiver {
        Type InitType { get; }
    }
    public interface IInitReceiver<T> : IInitReceiver {
        void Init(Entity entity, T value);
    }

    public class Template {
        internal interface IEntry {
            IEntry Clone();
            Type InitType { get; }
            void Initialize(IInitReceiver[] receivers);
            void RunAllocators(Entity entity);
            void RunAllocatorsWithValue(Entity entity, IEntry valueHolder);
        }
        internal class Entry<T> : IEntry {
            private T value_;
            private IInitReceiver<T>[] receivers_;

            public Entry(T value) {
                value_ = value;
            }

            IEntry IEntry.Clone() {
                return new Entry<T>(value_) {
                    receivers_ = receivers_
                };
            }

            Type IEntry.InitType => typeof(T);
            void IEntry.Initialize(IInitReceiver[] receivers) {
                if (receivers_ != null)
                    return;
                if (!receivers.Any(r => r.InitType == typeof(T)))
                    throw new InvalidOperationException($"{typeof(T)} is not a valid initialization type; nothing uses it.");
                receivers_ = receivers.OfType<IInitReceiver<T>>().ToArray();
            }

            void IEntry.RunAllocators(Entity entity) {
                foreach (IInitReceiver<T> receiver in receivers_)
                    receiver.Init(entity, value_);
            }

            void IEntry.RunAllocatorsWithValue(Entity entity, IEntry valueHolder_) {
                var valueHolder = (Entry<T>)valueHolder_;
                foreach (IInitReceiver<T> receiver in receivers_)
                    receiver.Init(entity, valueHolder.value_);
            }
        }


        private IEntry[] entries_;
        private bool initialized_;

        private Template(IEntry[] entries) {
            entries_ = entries;
        }

        internal void RunAllocators(IInitReceiver[] initReceivers, Entity entity, Overrides overrides) {
            if (!initialized_) {
                initialized_ = true;
                InitializeCachedAllocators(initReceivers);
            }

            // Use a fast-path when there is no override data.
            if (overrides == null || overrides.entries_.Count == 0) {
                foreach (IEntry entry in entries_)
                    entry.RunAllocators(entity);
            }
            else {
                foreach (IEntry entry in entries_) {
                    IEntry overrideEntry;
                    if (overrides.entries_.TryGetValue(entry.InitType, out overrideEntry))
                        entry.RunAllocatorsWithValue(entity, overrideEntry);
                    else
                        entry.RunAllocators(entity);
                }
            }
        }

        private void InitializeCachedAllocators(IInitReceiver[] initReceivers) {
            foreach (IEntry entry in entries_)
                entry.Initialize(initReceivers);
        }

        public bool HasDataFor(Type type) => entries_.Any(e => e.InitType == type);

        // TODO: Consider deprecating Override and only supporting Extend. This would let
        //       us implement things like `Filter.Predicate(t => t.Health > 10)` which
        //       would be extremely useful. Note that the Filter wouldn't really work as
        //       expected, since it is not dynamically updated according to object state.
        //
        //       This could be a performance issue if the user is constanting Extending
        //       templates. Maybe we should disallow dynamic Template creation?

        /// <summary>
        /// Override an existing template. Overriding the template reuses the
        /// initialization state so it is fast.
        /// </summary>
        public Overrides Override() => new Overrides(this);

        /// <summary>
        /// Extend a template. This allows adding and removing data instances.
        /// It is more expensive than overridding a template.
        /// </summary>
        public Builder Extend() => new Builder(this);

        public class Overrides {
            internal Template template_;
            internal Dictionary<Type, IEntry> entries_;

            internal Overrides(Template template) {
                template_ = template;
                entries_ = new Dictionary<Type, IEntry>();
            }

            public Overrides Add<T>(T value) {
                // This restriction is so we can cache the set of filters that an entity is
                // added /removed from.
                if (!template_.HasDataFor(typeof(T)))
                    throw new InvalidOperationException($"{typeof(T)} cannot be overridden because it does not exist in the original template.");

                entries_[typeof(T)] = new Entry<T>(value);
                return this;
            }
        }

        public static Builder Build() {
            return new Builder();
        }

        public class Builder {
            private List<IEntry> entries_;

            internal Builder() {
                entries_ = new List<IEntry>();
            }

            internal Builder(Template template) {
                entries_ = new List<IEntry>(template.entries_.Length);
                foreach (IEntry entry in template.entries_)
                    entries_.Add(entry.Clone());
            }

            public Builder Add<T>(T value) {
                entries_.Add(new Entry<T>(value));
                return this;
            }

            public static implicit operator Template(Builder builder) {
                return new Template(builder.entries_.ToArray());
            }
        }
    }
}