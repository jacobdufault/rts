﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Forge2 {
    // Used for maintaing a set of entities which contain a given set of data.

    public class FilterManager : IEntityCreated, IEntityDestroyed {
        private Filter[] filters_;

        private struct PrecomputedTemplate {
            public Template template;
            public Filter[] filters;
        }
        private List<PrecomputedTemplate> precomputedTemplates_;
        private DenseEntityDictionary<int> entityToTemplateIdx_;
        private Dictionary<Type, Type> dataToInitType_;

        public FilterManager(Dictionary<Type, Type> dataToInitType, HashSet<Type> storedFrameTypes, IEnumerable<Filter> filters) {
            dataToInitType_ = dataToInitType;

            filters_ = filters.ToArray();
            foreach (Filter filter in filters) {
                filter.entities = new Bag<Entity>();
                filter.Verify(storedFrameTypes);
            }


            precomputedTemplates_ = new List<PrecomputedTemplate>();
            entityToTemplateIdx_ = new DenseEntityDictionary<int>();
        }

        private int GetPrecomputedIdxForTemplate(Template template) {
            // Try to find existing.
            for (int i = 0; i < precomputedTemplates_.Count; ++i) {
                if (precomputedTemplates_[i].template == template)
                    return i;
            }

            // Create new.
            return BuildPrecomputedIdx(template);
        }

        private int BuildPrecomputedIdx(Template template) {
            var precomputed = new PrecomputedTemplate() {
                template = template,
                filters = filters_.Where(f => f.Satifies(template, dataToInitType_)).ToArray()
            };
            precomputedTemplates_.Add(precomputed);
            return precomputedTemplates_.Count - 1;
        }

        void IEntityCreated.OnCreated(Entity entity, Template template, Template.Overrides overrides) {
            int idx = GetPrecomputedIdxForTemplate(template);
            entityToTemplateIdx_.Set(entity, idx);
            foreach (Filter filter in precomputedTemplates_[idx].filters)
                filter.entities.Add(entity);
        }

        void IEntityDestroyed.OnDestroyed(List<Entity> destroyed) {
            foreach (Entity entity in destroyed) {
                int idx = entityToTemplateIdx_[entity];
                foreach (Filter filter in precomputedTemplates_[idx].filters)
                    filter.entities.Remove(entity);
            }
        }
    }

    public class Filter : IEnumerable<Entity> {
        private Type[] allOf_;
        private Type[][] anyOf_;
        private Type[] noneOf_;

        private Filter[] isTrue_;
        private Filter[] isFalse_;

        /// <summary>
        /// All of the entities in a bag. This is instantiated when the Filter is registered
        /// to the framework, so if this is null it means that the the Filter is not getting
        /// registered. This means that no entities will ever be added.
        /// </summary>
        public Bag<Entity> entities {
            get {
                if (entities_ == null)
                    throw new InvalidOperationException("Filter was not registered to the framework");
                return entities_;
            }
            set {
                entities_ = value;
            }
        }
        private Bag<Entity> entities_;

        private Filter(Type[] allOf, Type[][] anyOf, Type[] noneOf, Filter[] isTrue, Filter[] isFalse) {
            allOf_ = allOf;
            anyOf_ = anyOf;
            noneOf_ = noneOf;
            isTrue_ = isTrue;
            isFalse_ = isFalse;
        }

        /// <summary>
        /// Verifies that this filter does not contain any types which are not in storedFrameTypes.
        /// </summary>
        internal void Verify(HashSet<Type> storedFrameTypes) {
            void VerifyArray(Type[] types)
            {
                foreach (Type type in types) {
                    if (!storedFrameTypes.Contains(type))
                        throw new InvalidOperationException($"No Frame stores data of type {type}");
                }
            }

            VerifyArray(allOf_);
            foreach (Type[] types in anyOf_)
                VerifyArray(types);
            VerifyArray(noneOf_);
            foreach (Filter filter in isTrue_)
                filter.Verify(storedFrameTypes);
            foreach (Filter filter in isFalse_)
                filter.Verify(storedFrameTypes);
        }

        /// <summary>
        /// Returns true if this filter wants to track entity instances created by
        /// the given template.
        /// </summary>
        internal bool Satifies(Template template, Dictionary<Type, Type> dataToInitType) {
            // Check allOf_.
            foreach (Type type in allOf_) {
                if (!template.HasDataFor(dataToInitType[type]))
                    return false;
            }

            // Check anyOf_.
            foreach (Type[] types in anyOf_) {
                bool contained = false;
                foreach (Type type in types) {
                    // TODO: Maybe an if statement will be faster?
                    contained = contained || template.HasDataFor(dataToInitType[type]);
                }
                if (!contained)
                    return false;
            }

            // Check noneOf_.
            foreach (Type type in noneOf_) {
                if (template.HasDataFor(dataToInitType[type]))
                    return false;
            }

            // Check child filters.
            foreach (Filter filter in isTrue_) {
                if (!filter.Satifies(template, dataToInitType))
                    return false;
            }
            foreach (Filter filter in isFalse_) {
                if (filter.Satifies(template, dataToInitType))
                    return false;
            }
            return true;
        }

        public override String ToString() {
            string ToStr(Type[] typeIds)
            {
                return "{" + String.Join(", ", typeIds.Select(t => t.ToString()).ToArray()) + "}";
            }
            string ToStrArr(Type[][] typeIds)
            {
                return "{" + String.Join(", ", typeIds.Select(ToStr).ToArray()) + "}";
            }

            string allOf = "";
            if (allOf_.Length > 0) allOf = $"allOf: {ToStr(allOf_)}";
            string anyOf = "";
            if (anyOf_.Length > 0) anyOf = $"allOf: {ToStrArr(anyOf_)}";
            string noneOf = "";
            if (noneOf_.Length > 0) noneOf = $"allOf: {ToStr(noneOf_)}";
            string isTrue = "";
            if (isTrue_.Length > 0) isTrue = $"isTrue: " + String.Join(", ", isTrue_.Select(t => t.ToString()).ToArray());
            string isFalse = "";
            if (isFalse_.Length > 0) isFalse = $"isTrue: " + String.Join(", ", isFalse_.Select(t => t.ToString()).ToArray());

            string entries = String.Join(", ", new[] { allOf, anyOf, noneOf, isTrue, isFalse }.Where(t => !string.IsNullOrEmpty(t)).ToArray());
            return $"Filter [{entries}]";
        }

        IEnumerator<Entity> IEnumerable<Entity>.GetEnumerator() => entities.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => entities.GetEnumerator();
        public ArrayEnumerator<Entity> GetEnumerator() => entities.GetEnumerator();

        public static Filter Empty() {
            return new Filter(new Type[0], new Type[0][], new Type[0], new Filter[0], new Filter[0]);
        }

        // All of the types.
        public static Builder AllOf<T0>() { return AllOf(typeof(T0)); }
        public static Builder AllOf<T0, T1>() { return AllOf(typeof(T0), typeof(T1)); }
        public static Builder AllOf<T0, T1, T2>() { return AllOf(typeof(T0), typeof(T1), typeof(T2)); }
        public static Builder AllOf<T0, T1, T2, T3>() { return AllOf(typeof(T0), typeof(T1), typeof(T2), typeof(T3)); }
        private static Builder AllOf(params Type[] types) {
            return (new Builder()).AllOf(types);
        }

        // At least *one* of the given types.
        public static Builder AnyOf<T0>() { return AnyOf(typeof(T0)); }
        public static Builder AnyOf<T0, T1>() { return AnyOf(typeof(T0), typeof(T1)); }
        public static Builder AnyOf<T0, T1, T2>() { return AnyOf(typeof(T0), typeof(T1), typeof(T2)); }
        public static Builder AnyOf<T0, T1, T2, T3>() { return AnyOf(typeof(T0), typeof(T1), typeof(T2), typeof(T3)); }
        private static Builder AnyOf(params Type[] types) {
            return (new Builder()).AnyOf(types);
        }

        // None of the types.
        public static Builder NoneOf<T0>() { return NoneOf(typeof(T0)); }
        public static Builder NoneOf<T0, T1>() { return NoneOf(typeof(T0), typeof(T1)); }
        public static Builder NoneOf<T0, T1, T2>() { return NoneOf(typeof(T0), typeof(T1), typeof(T2)); }
        public static Builder NoneOf<T0, T1, T2, T3>() { return NoneOf(typeof(T0), typeof(T1), typeof(T2), typeof(T3)); }
        private static Builder NoneOf(params Type[] types) {
            return (new Builder()).NoneOf(types);
        }

        // The child filter must return either true or false.
        public static Builder IsTrue(Filter filter) {
            return (new Builder()).IsTrue(filter);
        }
        public static Builder IsFalse(Filter filter) {
            return (new Builder()).IsFalse(filter);
        }

        public class Builder {
            private List<Type> allOf_ = new List<Type>();
            private List<Type[]> anyOf_ = new List<Type[]>();
            private List<Type> noneOf_ = new List<Type>();
            private List<Filter> isTrue_ = new List<Filter>();
            private List<Filter> isFalse_ = new List<Filter>();

            // All of the types.
            public Builder AllOf<T0>() { return AllOf(typeof(T0)); }
            public Builder AllOf<T0, T1>() { return AllOf(typeof(T0), typeof(T1)); }
            public Builder AllOf<T0, T1, T2>() { return AllOf(typeof(T0), typeof(T1), typeof(T2)); }
            public Builder AllOf<T0, T1, T2, T3>() { return AllOf(typeof(T0), typeof(T1), typeof(T2), typeof(T3)); }
            internal Builder AllOf(params Type[] types) {
                allOf_.AddRange(types);
                return this;
            }

            // At least *one* of the given types.
            public Builder AnyOf<T0>() { return AnyOf(typeof(T0)); }
            public Builder AnyOf<T0, T1>() { return AnyOf(typeof(T0), typeof(T1)); }
            public Builder AnyOf<T0, T1, T2>() { return AnyOf(typeof(T0), typeof(T1), typeof(T2)); }
            public Builder AnyOf<T0, T1, T2, T3>() { return AnyOf(typeof(T0), typeof(T1), typeof(T2), typeof(T3)); }
            internal Builder AnyOf(params Type[] types) {
                anyOf_.Add(types);
                return this;
            }

            // None of the types.
            public Builder NoneOf<T0>() { return NoneOf(typeof(T0)); }
            public Builder NoneOf<T0, T1>() { return NoneOf(typeof(T0), typeof(T1)); }
            public Builder NoneOf<T0, T1, T2>() { return NoneOf(typeof(T0), typeof(T1), typeof(T2)); }
            public Builder NoneOf<T0, T1, T2, T3>() { return NoneOf(typeof(T0), typeof(T1), typeof(T2), typeof(T3)); }
            internal Builder NoneOf(params Type[] types) {
                noneOf_.AddRange(types);
                return this;
            }

            // The child filter must return either true or false.
            public Builder IsTrue(Filter filter) {
                isTrue_.Add(filter);
                return this;
            }
            public Builder IsFalse(Filter filter) {
                isFalse_.Add(filter);
                return this;
            }

            private Filter Build() {
                return new Filter(allOf_.ToArray(), anyOf_.ToArray(), noneOf_.ToArray(), isTrue_.ToArray(), isFalse_.ToArray());
            }
            public static implicit operator Filter(Builder builder) {
                return builder.Build();
            }
        }
    }
}
