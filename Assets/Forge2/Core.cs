﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Forge2 {
    public class Core {
        public EntityManager entities;
        public FrameManager frames;
        public SystemManager systems;

        public Core(FrameManager frames, SystemManager systems, IEnumerable<Filter> additionalFilters) {
            this.frames = frames;
            this.systems = systems;

            // Build various lookup caches.
            var initReceivers = frames.frames.OfType<IInitReceiver>().ToArray();
            var dataToInitType = new Dictionary<Type, Type>();
            foreach (IFrame frame in frames.frames) {
                if (frame is IInitReceiver) {
                    Type dataType = frame.DataType;
                    Type initType = ((IInitReceiver)frame).InitType;
                    dataToInitType[dataType] = initType;
                }
            }
            var storedFrameTypes = new HashSet<Type>();
            foreach (IFrame frame in frames.frames)
                storedFrameTypes.Add(frame.DataType);

            // Build the rest of the system.
            var filterManager = new FilterManager(dataToInitType, storedFrameTypes, systems.allFilters.Concat(additionalFilters));
            entities = new EntityManager(initReceivers, frames, filterManager);
            systems.Initialize(entities, frames);
        }

        public void Update() {
            entities.RunCreateDestroy();
            frames.AdvanceFrame();
            systems.Update();
        }

        public class Builder {
            public SystemManager.Builder systems = new SystemManager.Builder();
            public FrameManager.Builder frames = new FrameManager.Builder();
            /// <summary>
            /// Add any filters which will not be auto-registered (ie, those inside of a BaseSystem
            /// derived class annotated with [AutoRegister]).
            /// </summary>
            public List<Filter> additionalFilters = new List<Filter>();

            public Core Build() {
                return new Core(frames.Build(), systems.Build(), additionalFilters);
            }
        }
    }
}
