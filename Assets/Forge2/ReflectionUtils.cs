﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Forge2 {
    /// <summary>
    /// Determines if the filter instance should be automatically injected into the owning framework.
    /// <br />
    /// This attribute is required on all Filter fields for code-clarity purposes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class AutoRegisterAttribute : Attribute {
        public AutoRegisterState state;

        public AutoRegisterAttribute() : this(AutoRegisterState.Register) { }
        public AutoRegisterAttribute(AutoRegisterState state) {
            this.state = state;
        }
    }

    public enum AutoRegisterState {
        Register,
        DoNotRegister
    }

    public static class ReflectionUtils {
        public static IEnumerable<FieldInfo> GetFields(Type type) {
            while (type != null) {
                FieldInfo[] fields = type.GetFields(
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                foreach (FieldInfo field in fields)
                    yield return field;

                type = type.BaseType;
            }
        }

        private static bool ShouldRegister(FieldInfo field) {
            object[] attributes = field.GetCustomAttributes(typeof(AutoRegisterAttribute), inherit: true);
            if (attributes.Length != 1)
                throw new InvalidOperationException($"{field.DeclaringType}.{field.Name} needs to be annotated with exactly one [{nameof(AutoRegisterAttribute)}] attribute.");
            return ((AutoRegisterAttribute)attributes[0]).state == AutoRegisterState.Register;
        }

        public static IEnumerable<Filter> FindFiltersOnObject(object obj) {
            foreach (FieldInfo field in GetFields(obj.GetType())) {
                if (field.FieldType == typeof(Filter)) {
                    if (!ShouldRegister(field))
                        continue;

                    var filter = (Filter)field.GetValue(obj);
                    if (filter == null)
                        throw new InvalidOperationException($"{obj.GetType()}.{field.Name} is null; please instantiate it.");

                    yield return filter;
                }
            }
        }
    }
}
