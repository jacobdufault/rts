﻿using System;

namespace Forge2 {

    // TODO: Segment 32-bit entity data into different groups. For example,
    //       Props, Environment, Units
    //
    //       Each group has id starting from 0. This will mean things like
    //       Dense storage are much faster since the maximum entity size is
    //       significantly smaller.
    public struct Entity : IComparable<Entity>, IEquatable<Entity> {
        // A semi-stable value that can be used to cross-reference state between
        // different storage locations. The entity id is **not** stable between
        // different updates.
        public readonly int id;

        public Entity(int id) {
            this.id = id;
        }

        public override string ToString() => $"Entity [id={id}]";

        public override bool Equals(object obj) {
            if (obj is Entity)
                return Equals((Entity)obj);
            return false;
        }

        public bool Equals(Entity other) => id == other.id;
        public override int GetHashCode() => id.GetHashCode();
        public static bool operator ==(Entity a, Entity b) => a.Equals(b);
        public static bool operator !=(Entity a, Entity b) => !a.Equals(b);
        int IComparable<Entity>.CompareTo(Entity other) => id.CompareTo(other.id);
    }
}
