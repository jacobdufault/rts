﻿namespace Forge2 {
    public struct Buffered<T> {
        public T active;
        public T inactive;

        public Buffered(T a, T b) {
            active = a;
            inactive = b;
        }

        public void Swap() {
            T tmp = active;
            active = inactive;
            inactive = tmp;
        }
    }

    public static class Buffered {
        public static Buffered<T> AutoCreate<T>() where T : new() {
            return new Buffered<T>(new T(), new T());
        }
    }

}
