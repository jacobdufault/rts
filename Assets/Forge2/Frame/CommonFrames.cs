﻿using System;
using System.Collections.Generic;

namespace Forge2 {
    public abstract class DictionaryFrame<TEntityDictionary, T, TInit> :
        IFrame, IInitReceiver<TInit>, IEntityDestroyed, ICopyable
        where TEntityDictionary : IEntityDictionary<T>, new()
        where T : struct {

        void IFrame.OnFrameCompleted() { }

        Type IFrame.DataType => typeof(T);
        Type IInitReceiver.InitType => typeof(TInit);
        void IInitReceiver<TInit>.Init(Entity entity, TInit value) {
            Init(entity, value, ref storage.GetOrInit(entity));
        }

        void IEntityDestroyed.OnDestroyed(List<Entity> destroyed) {
            foreach (Entity entity in destroyed)
                storage.Remove(entity);
        }

        void ICopyable.CopyTo(ICopyable other_) {
            var other = (DictionaryFrame<TEntityDictionary, T, TInit>)other_;
            storage.CopyTo(other.storage);
        }

        public TEntityDictionary storage = new TEntityDictionary();
        public ref T this[Entity entity] => ref storage[entity];

        protected abstract void Init(Entity entity, TInit initData, ref T frameData);
    }

    // TODO: We should deprecate DenseFrame since with 1million+ entities it won't make sense.
    // TODO: Alternatively we could store all of the data in a flat unordered array but keep an ordered
    //       mapping.
    public abstract class DenseFrame<T, TInit> : DictionaryFrame<DenseEntityDictionary<T>, T, TInit>
        where T : struct { }
    public abstract class DenseFrame<T> : DenseFrame<T, T> where T : struct {
        protected override void Init(Entity entity, T initData, ref T frameData) {
            frameData = initData;
        }
    }

    public abstract class SparseFrame<T, TInit> : DictionaryFrame<SparseEntityDictionary<T>, T, TInit>
        where T : struct { }
    public abstract class SparseFrame<T> : SparseFrame<T, T> where T : struct {
        protected override void Init(Entity entity, T initData, ref T frameData) {
            frameData = initData;
        }
    }


    public interface ICopyable {
        void CopyTo(ICopyable other);
    }
    public abstract class VersionedFrame<TFrame, T, TInit> : IFrame, IInitReceiver<TInit>
        where TFrame : ICopyable, IInitReceiver<TInit> {

        private Buffered<TFrame> frames_;
        public TFrame readable => frames_.inactive;
        public TFrame writable => frames_.active;

        protected virtual bool InitializableWritableWithReadable => true;

        public VersionedFrame() {
            frames_ = AllocateFrames();
        }

        void IFrame.OnFrameCompleted() {
            frames_.Swap();
            if (InitializableWritableWithReadable)
                readable.CopyTo(writable);
        }

        Type IFrame.DataType => typeof(T);
        Type IInitReceiver.InitType => typeof(TInit);
        void IInitReceiver<TInit>.Init(Entity entity, TInit obj) {
            // If InitializableWritableWithReadable is true, then readable
            // will get the expected value from OnFrameCompleted.
            if (!InitializableWritableWithReadable)
                readable.Init(entity, default(TInit));

            writable.Init(entity, obj);
        }

        protected abstract Buffered<TFrame> AllocateFrames();
    }

    public abstract class DenseVersionedFrame<T, TInit> :
        VersionedFrame<DenseVersionedFrame<T, TInit>.Dense, T, TInit>
        where T : struct {

        protected abstract void Init(TInit initData, ref T frameData);

        protected override Buffered<Dense> AllocateFrames() {
            return new Buffered<Dense>(new Dense(this), new Dense(this));
        }

        public class Dense : DenseFrame<T, TInit> {
            private DenseVersionedFrame<T, TInit> context_;

            public Dense(DenseVersionedFrame<T, TInit> context) {
                context_ = context;
            }

            protected override void Init(Entity entity, TInit initData, ref T frameData) {
                context_.Init(initData, ref frameData);
            }
        }
    }
    public class DenseVersionedFrame<T> : DenseVersionedFrame<T, T> where T : struct {
        protected override void Init(T initData, ref T frameData) {
            frameData = initData;
        }
    }


    public abstract class SparseVersionedFrame<T, TInit> :
        VersionedFrame<SparseVersionedFrame<T, TInit>.Sparse, T, TInit>
        where T : struct {

        protected abstract void Init(TInit initData, ref T frameData);

        protected override Buffered<Sparse> AllocateFrames() {
            return new Buffered<Sparse>(new Sparse(this), new Sparse(this));
        }

        public class Sparse : SparseFrame<T, TInit> {
            private SparseVersionedFrame<T, TInit> context_;

            public Sparse(SparseVersionedFrame<T, TInit> context) {
                context_ = context;
            }

            protected override void Init(Entity entity, TInit initData, ref T frameData) {
                context_.Init(initData, ref frameData);
            }
        }
    }
    public class SparseVersionedFrame<T> : SparseVersionedFrame<T, T> where T : struct {
        protected override void Init(T initData, ref T frameData) {
            frameData = initData;
        }
    }
}
