﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Forge2 {
    public class FrameManager {
        public IFrame[] frames;

        public FrameManager(IEnumerable<IFrame> frames) {
            this.frames = frames.ToArray();
        }

        public T GetFrame<T>() where T : IFrame {
            foreach (IFrame frame in frames) {
                if (frame is T)
                    return (T)frame;
            }

            throw new InvalidOperationException($"No frame with type {typeof(T)}");
        }

        public IFrame GetFrame(Type type) {
            foreach (IFrame frame in frames) {
                if (frame.GetType() == type)
                    return frame;
            }

            throw new InvalidOperationException($"No frame with type {type}");
        }

        internal void AdvanceFrame() {
            foreach (IFrame frame in frames)
                frame.OnFrameCompleted();
        }

        public class Builder {
            private List<IFrame> entries = new List<IFrame>();

            public Builder Add<T>(T instance) where T : IFrame {
                entries.Add(instance);
                return this;
            }

            public Builder Add<T>() where T : IFrame, new() {
                return Add(new T());
            }

            public Builder Add(IFrame frame) {
                entries.Add(frame);
                return this;
            }

            public FrameManager Build() {
                return new FrameManager(entries);
            }
        }
    }
}
