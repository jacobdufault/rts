﻿using System;
using Forge2.Internal;

namespace Forge2.Internal {
    public abstract class BaseUnorderedDenseFrame<T> : IFrame where T : struct {
        void IFrame.OnFrameCompleted() { }
        Type IFrame.DataType => typeof(T);

        public Bag<T> storage = new Bag<T>();
    }
}

namespace Forge2 {
    public abstract class UnorderedDenseFrame<T, TInit> : BaseUnorderedDenseFrame<T>, IInitReceiver<TInit> where T : struct {
        Type IInitReceiver.InitType => typeof(TInit);
        void IInitReceiver<TInit>.Init(Entity entity, TInit value) {
            storage.Add(default(T));
            Initialize(entity, value, ref storage[storage.Length - 1]);
        }

        protected abstract void Initialize(Entity entity, TInit initData, ref T frameData);
    }

    public class UnorderedDenseFrame<T> : BaseUnorderedDenseFrame<T> where T : struct { }
}
