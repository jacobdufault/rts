﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Forge2 {
    /// <summary>
    /// An unordered collection of items.
    /// </summary>
    /// <remarks>
    /// If this class is allocating (ie, Remove is getting called without an index),
    /// then make sure T extends IEquatable{T}.
    /// </remarks>
    public sealed class Bag<T> : IEnumerable<Element<T>>, IEnumerable<T> {
        public T[] RawItems;

        public int Length {
            get;
            private set;
        }

        public Bag()
            : this(8) {
        }

        public Bag(int capacity) {
            RawItems = new T[capacity];
            Length = 0;
        }

        /// <summary>
        /// Copies from the given bag into this one.
        /// </summary>
        /// <param name="bag">The bag to copy from</param>
        public void CopyFrom(Bag<T> bag) {
            // Increase capacity if necessary
            if (bag.Length > RawItems.Length) {
                Array.Resize(ref RawItems, bag.Length);
            }

            // Copy items over
            Array.Copy(bag.RawItems, RawItems, bag.Length);

            // Clear out old items
            if (Length > bag.Length) {
                Array.Clear(RawItems, bag.Length, Length - bag.Length);
            }

            // Update length
            Length = bag.Length;
        }

        /// <summary>
        /// Creates a duplicate of this bag that has a different backing array.
        /// </summary>
        public Bag<T> Copy() {
            var bag = new Bag<T>(RawItems.Length);
            Array.Copy(RawItems, bag.RawItems, Length);
            return bag;
        }

        /// <summary>
        /// Returns the index of the given item in the bag, or -1 if it is not found.
        /// </summary>
        public int IndexOf(T item) {
            return Array.IndexOf(RawItems, item, 0, Length);
        }

        /// <summary>
        /// Removes the item at given index from the bag in O(1) time. This operation does not
        /// maintain the order of elements inside of the bag!
        /// </summary>
        public void Remove(int index) {
            // swap
            RawItems[index] = RawItems[Length - 1];

            // reduce length
            RawItems[Length - 1] = default(T);
            --Length;
        }

        /// <summary>
        /// Clears all stored items from this instance.
        /// </summary>
        public void Clear() {
            Array.Clear(RawItems, 0, Length);
            Length = 0;
        }

        /// <summary>
        /// Clears all stored items, but does not reset the array
        /// values to the default.
        /// </summary>
        public void ClearWithoutReset() {
            Length = 0;
        }

        /// <summary>
        /// Remove the item from the bag. This is O(n) and has to scan the bag to find the item.
        /// </summary>
        /// <returns>True if the item was found and removed, false otherwise.</returns>
        public bool Remove(T item) {
            int index = IndexOf(item);
            if (index == -1) {
                return false;
            }

            Remove(index);
            return true;
        }

        public void Add(T value) {
            if (Length == RawItems.Length) {
                Array.Resize(ref RawItems, (int)((RawItems.Length + 1) * 1.5));
            }

            RawItems[Length++] = value;
        }

        public void AddRange(IEnumerable<T> values) {
            foreach (T value in values)
                Add(value);
        }

        public ref T this[int index] => ref RawItems[index];

        /// <summary>
        /// Returns true if the Bag contains an instance of the given item.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <returns>True if it is in the bag, false otherwise.</returns>
        public bool Contains(T item) {
            return IndexOf(item) >= 0;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => GetEnumerator();
        IEnumerator<Element<T>> IEnumerable<Element<T>>.GetEnumerator() => GetFastEnumerator();
        public ArrayEnumerator<T> GetEnumerator() => new ArrayEnumerator<T>(RawItems, Length);
        public ElementArrayEnumerator<T> GetFastEnumerator() => new ElementArrayEnumerator<T>(RawItems, Length);
    }
}