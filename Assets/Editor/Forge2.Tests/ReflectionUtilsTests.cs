﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Forge2 {
    public class ReflectionUtilsTests {
        public class HasFilter {
            [AutoRegister]
            private Filter parent = Filter.Empty();
        }

        public class HasParentFilter : HasFilter {
            [AutoRegister]
            private Filter child = Filter.Empty();
        }

        public class HasNullFilter {
#pragma warning disable 0169 // unused variable
            [AutoRegister]
            private Filter none;
#pragma warning restore 0169
        }

        public class MissingAutoRegister {
            private Filter foo = Filter.Empty();
        }

        [Test]
        public void TestGetFilters() {
            Assert.AreEqual(1, ReflectionUtils.FindFiltersOnObject(new HasFilter()).Count());
            Assert.AreEqual(2, ReflectionUtils.FindFiltersOnObject(new HasParentFilter()).Count());
        }

        [Test]
        public void TestNullFiltersThrowException() {
            Assert.Throws<InvalidOperationException>(() => {
                ReflectionUtils.FindFiltersOnObject(new HasNullFilter()).Count();
            });
        }

        [Test]
        public void TestNoAutoRegisterThrowsException() {
            Assert.Throws<InvalidOperationException>(() => {
                ReflectionUtils.FindFiltersOnObject(new MissingAutoRegister()).Count();
            });
        }
    }
}
