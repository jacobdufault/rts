﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Forge2 {
    // There are two big ideas in this design:
    //
    // 1. N frames of state, so we can easily parallelize. We never write to frame N, we only read.
    //    Frame N+1 starts out as a memcpy of Frame N.
    //
    // 2. Clever entity architecture.
    //
    //
    // For #1, we need a way to ensure we can memcpy the entire state.

    //
    //
    // UI thread: position system, gfx, Unity things
    // [Game threads]: Lots of independent systems
    //
    // tick:sync:tick:sync
    //
    // So we are as slow as the slowest possible system.
    //
    // What if a system wants to communicate? For example,
    // to share positioning.
    //
    //
    //
    // Basically, we have two systems, A and B. They need
    // to share state between them. We could have a generic (frame dest) system,
    // where frame n+1 is determined entirely by frame n. The simulation engine
    // has a view where frame 0..n are all entirely unique, but we only store
    // say 5 frames, and rotate them around.
    //
    // So long as we only write to the new frame, there should not be a big problem here.
    //
    // If we have this architecture, then we can store lists of data, for example,
    // PositionState[]. We can write a job that executes on the current frame's position state
    // to drive the next frame. Other jobs can similarily fetch the previous frame's data as needed.
    //
    //

    public struct Damage {
        // TODO: This frame should not support initialization
        public class FrameStorage : UnorderedDenseFrame<Damage> { }

        public int amount;
        public Entity destination;
        public Entity source;
    }

    public class ApplyDamage : BaseSystem {
        private Damage.FrameStorage damageFrame;
        private HealthData.FrameStorage healthFrame;

        protected override void OnInitialized() {
            GetFrame(out damageFrame);
            GetFrame(out healthFrame);
        }

        public override void Update() {
            var wHealthFrame = healthFrame.writable;

            foreach (Damage damage in damageFrame.storage) {
                if (!wHealthFrame.storage.Has(damage.destination))
                    throw new InvalidOperationException("Damage applied to entity with no health");
                wHealthFrame[damage.destination].health -= damage.amount;
            }

            damageFrame.storage.Clear();
        }
    }

    public struct DamageRandomEntity {
        public class FrameStorage : UnorderedDenseFrame<DamageRandomEntity, DamageRandomEntity> {
            protected override void Initialize(Entity entity, DamageRandomEntity initData, ref DamageRandomEntity frameData) {
                frameData = initData;
                frameData.owner = entity;
            }
        }

        public Entity owner;
        public int amount;
    }

    public class RunDamageRandomEntitySystem : BaseSystem {
        [AutoRegister]
        private Filter candidates = Filter.AllOf<HealthData>();
        private DamageRandomEntity.FrameStorage damageRandom;
        private Damage.FrameStorage postDamage;

        private Random random = new Random();

        protected override void OnInitialized() {
            GetFrame(out damageRandom);
            GetFrame(out postDamage);
        }

        public override void Update() {
            if (candidates.entities.Length == 0)
                return;

            foreach (DamageRandomEntity data in damageRandom.storage) {
                // Find an entity to target that is *not* the originator.
                int destination;
                while (true) {
                    destination = random.Next(candidates.entities.Length);
                    if (destination != data.owner.id)
                        break;
                }

                // Post damage to get processed later.
                postDamage.storage.Add(new Damage {
                    amount = data.amount,
                    source = data.owner,
                    destination = new Entity(destination)
                });
            }
        }

    }


    public struct UniqueId {
        public class FrameStorage : DenseFrame<UniqueId> {
            private int nextUniqueId = 0;

            protected override void Init(Entity entity, UniqueId initData, ref UniqueId frameData) {
                frameData.uniqueId = nextUniqueId++;
            }
        }

        public int uniqueId;

        public override String ToString() {
            return uniqueId.ToString();
        }
    }

    public struct NameData {
        public class FrameStorage : DenseFrame<NameData> { }

        public NameData(string name) {
            this.name = name;
        }

        public string name;

        public override string ToString() {
            return name;
        }
    }

    public struct PositionData {
        public class FrameStorage : DenseVersionedFrame<PositionData, Init> {
            protected override bool InitializableWritableWithReadable => false;

            protected override void Init(Init initData, ref PositionData frameData) {
                frameData.x = initData.x;
                frameData.y = initData.y;
                frameData.z = initData.x + initData.y;
            }
        }

        public struct Init {
            public float x, y;

            public Init(float x, float y) {
                this.x = x;
                this.y = y;
            }
        }

        public float x, y, z;
    }

    public struct HealthData {
        public class FrameStorage : SparseVersionedFrame<HealthData> { }

        public HealthData(int health) {
            this.health = health;
        }

        public int health;
    }
    public struct UnitData {
        public class FrameStorage : SparseVersionedFrame<UnitData> { }
    }
    public struct BuildingData {
        public class FrameStorage : SparseVersionedFrame<BuildingData> { }
    }

    public struct RegenData {
        public class FrameStorage : EmptyFrame<RegenData> { }
    }

    public class RegenBuildings : BaseSystem {
        [AutoRegister]
        public Filter toRegen = Filter
            .AllOf<HealthData, RegenData>()
            .AnyOf<UnitData, BuildingData>();
        private HealthData.FrameStorage healthFrame;

        protected override void OnInitialized() {
            GetFrame(out healthFrame);
        }

        public override void Update() {
            foreach (Entity entity in toRegen) {
                ref HealthData health = ref healthFrame.writable[entity];
                health.health += 3;
            }
        }

        public static Filter updateFilter = Filter
            .AllOf<HealthData>()
            .AnyOf<UnitData, BuildingData>();

        public static void UpdatePosition(EntityManager entityManager, FrameManager frameManager) {
            /*
            var positionReader = frameManager.Readable<PositionData.FrameStorage>();
            var positionWriter = frameManager.Writable<PositionData.FrameStorage>();
            var guidFrame = frameManager.Readable<GuidData.FrameStorage>();
            var nameFrame = frameManager.Readable<NameData.FrameStorage>();
            var healthFrame = frameManager.Readable<HealthData.FrameStorage>();

            for (int i = 0; i < positionWriter.linearData.Length; ++i) {
                positionWriter.linearData[i].x = positionWriter.linearData[i].x + 1;
            }

            foreach (Entity entity in updateFilter.entities) {
                Console.WriteLine($"Entity {entity} has name={nameFrame[entity].name}, guid={guidFrame[entity].guid}, health={healthFrame[entity].health}");
                PositionData currentPosition = positionReader[entity];
                ref var updatedPosition = ref positionWriter[entity];
                updatedPosition.x = currentPosition.x;
            }*/
        }
    }

    public class PrintNameSystem : BaseSystem {
        [AutoRegister]
        private Filter nameAndHealth = Filter.AllOf<HealthData, NameData, UniqueId>();
        private HealthData.FrameStorage healthReader;
        private NameData.FrameStorage nameReader;
        private UniqueId.FrameStorage ids;

        protected override void OnInitialized() {
            GetFrame(out healthReader);
            GetFrame(out nameReader);
            GetFrame(out ids);
        }

        public override void Update() {

            foreach (Entity entity in nameAndHealth) {
                int health = healthReader.readable[entity].health;
                string name = nameReader[entity].name;
                UniqueId uniqueId = ids[entity];

                //Console.WriteLine($"Entity({uniqueId}) {name} has {health} hp");
            }
        }
    }

    public static class FpControl {
        // https://msdn.microsoft.com/en-us/library/e9b52ceh.aspx
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static uint _controlfp(uint newcw, uint mask);

        // Denormal control.
        const uint _MCW_DN = 0x03000000;
        const uint _DN_SAVE = 0x00000000;
        const uint _DN_FLUSH = 0x01000000;

        // Interrupt exception mask.
        const uint _MCW_EM = 0x0008001f;
        const uint EM_INVALID = 0x00000010;
        const uint EM_DENORMAL = 0x00080000;
        const uint EM_ZERODIVIDE = 0x00000008;
        const uint EM_OVERFLOW = 0x00000004;
        const uint EM_UNDERFLOW = 0x00000002;
        const uint EM_INEXACT = 0x00000001;

        // Infinity control.
        const uint _MCW_IC = 0x00040000;
        const uint _IC_AFFINE = 0x00040000;
        const uint _IC_PROJECTIVE = 0x00000000;

        // Precision control.
        const uint _MCW_PC = 0x00030000;
        const uint _PC_24 = 0x00020000;
        const uint _PC_53 = 0x00010000;
        const uint _PC_64 = 0x00000000;

        // Rounding control.
        const uint _MCW_RC = 0x00000300;
        const uint _RC_CHOP = 0x00000300;
        const uint _RC_UP = 0x00000200;
        const uint _RC_DOWN = 0x00000100;
        const uint _RC_NEAR = 0x00000000;

        public static bool Is32Bit = IntPtr.Size == 4;
        public static void Print() {
            Print(_controlfp(0, 0));
        }
        public static void Print(uint control) {
            Console.WriteLine("_MCW_PC=" + (control & _MCW_PC));
            Console.WriteLine("_MCW_RC=" + (control & _MCW_RC));
        }
        public static void Ensure() {
            Console.WriteLine("32bit = " + Is32Bit);

            Print();
            Console.WriteLine("Changing FP mode");
            Print(_controlfp(_PC_24, _MCW_PC));
            Print(_controlfp(_RC_NEAR, _MCW_RC));
            Console.WriteLine("Done changing");
            Print();
        }

        public static void Assert() {
            if ((_controlfp(0, 0) & _MCW_PC) != _PC_24)
                throw new InvalidOperationException("floating point mode is not determinsitic");
            if ((_controlfp(0, 0) & _MCW_RC) != _RC_NEAR)
                throw new InvalidOperationException("floating point mode is not determinsitic");
        }
    }

    public class Program {
        public static void Main(string[] args) {
            FpControl.Ensure();
            Console.ReadLine();
            FpControl.Assert();

            var builder = new Core.Builder();

            builder.frames
                .Add<Damage.FrameStorage>()
                .Add<DamageRandomEntity.FrameStorage>()
                .Add<UniqueId.FrameStorage>()
                .Add<NameData.FrameStorage>()
                .Add<PositionData.FrameStorage>()
                .Add<HealthData.FrameStorage>()
                .Add<UnitData.FrameStorage>()
                .Add<BuildingData.FrameStorage>()
                .Add<RegenData.FrameStorage>();

            builder.systems
                .Add<RunDamageRandomEntitySystem>()
                .Add<ApplyDamage>()
                .Add<RegenBuildings>();

            Core core = builder.Build();
            core.Update();

            Template monster = Template.Build()
                .Add(new UniqueId())
                .Add(new NameData("Monster"))
                .Add(new HealthData(100))
                .Add(new DamageRandomEntity { amount = 1 });

            Template civilian = Template.Build()
                .Add(new UniqueId())
                .Add(new NameData("Civilian"))
                .Add(new HealthData(10))
                .Add(new UnitData())
                .Add(new RegenData());

            Template.Overrides john = civilian.Override()
                .Add(new NameData("John"));

            {
                Entity e0 = core.entities.Create(monster);
                Entity e1 = core.entities.Create(civilian);
                Entity e2 = core.entities.Create(john);
                core.Update();
                core.entities.Destroy(e0);
                core.entities.Destroy(e1);
                core.entities.Destroy(e2);
                core.Update();
            }

            for (int i = 0; i < 1; ++i)
                core.entities.Create(monster);
            for (int i = 0; i < 1; ++i)
                core.entities.Create(civilian);
            for (int i = 0; i < 25; ++i)
                core.entities.Create(john);

            for (int i = 0; i < 20; ++i) {
                //var stopwatch = Stopwatch.StartNew();

                core.Update();
                //stopwatch.Stop();
                //Console.WriteLine($"Update took {stopwatch.ElapsedMilliseconds} or {stopwatch.ElapsedTicks / (float)Stopwatch.Frequency * 1000.0} ms");
                //Console.WriteLine();
                //Console.WriteLine();
                //Console.WriteLine();
            }

#if false
            foreach (var kvp in core.frames.GetFrame<HealthData.FrameStorage>().readable.storage) {
                Entity entity = kvp.Key;
                HealthData health = kvp.Value;
                NameData name = core.frames.GetFrame<NameData.FrameStorage>()[entity];
                Console.WriteLine($"{entity} {name} has {health.health} health");
            }
#endif

            //Console.ReadLine();
        }
    }
}