﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Forge2 {
    [TestFixture]
    public class CommonTestFixture {
        protected Core core;
        protected FrameManager frameManager => core.frames;
        protected EntityManager entityManager => core.entities;
        protected SystemManager systemManager => core.systems;
        protected Template emptyTemplate = Template.Build();

        [SetUp]
        public void SetUp() {
            if (!DeferredInitialize)
                Initialize();
        }

        protected void Initialize(params Filter[] filters) {
            Initialize(new List<Filter>(filters));
        }

        protected void Initialize(List<Filter> filters) {
            var builder = new Core.Builder();
            builder.additionalFilters.AddRange(filters);

            SetupBuilder(builder);
            core = builder.Build();

            OnPostInitialize();
        }

        protected virtual void OnPostInitialize() { }

        protected virtual void SetupBuilder(Core.Builder builder) { }

        /// <summary>
        /// If set to true by a test, then the test methods will need to call Initialize() themselves.
        /// </summary>
        protected virtual bool DeferredInitialize => false;

        protected void Update() {
            core.Update();
        }

        protected void RunThenUpdate(Action before) {
            before();
            Update();
        }
    }
}
