﻿using System;
using NUnit.Framework;

namespace Forge2 {
    public class DataStructureTests {
        [Test]
        public void TestBufferedSwap() {
            var buffered = new Buffered<int>(1, 2);
            Assert.AreEqual(1, buffered.active);
            Assert.AreEqual(2, buffered.inactive);

            buffered.Swap();
            Assert.AreEqual(2, buffered.active);
            Assert.AreEqual(1, buffered.inactive);
        }
    }
}
