﻿using MarkLight;
using MarkLight.Views.UI;
using UnityEngine;

public class PerformanceView : Frame {
    public _float FPS;
    public _float DeltaTime;
    public _int UpdatesPerSecond;

    public _float SimulationTime;
    public _int EntityCount;

    public ViewSwitcher ViewSwitcher;

    private float updateFrequencyInSeconds_ => 1f / UpdatesPerSecond;
    private float updateAccumulator_ = 0;

    public static PerformanceView Instance;

    public void Awake() {
        Instance = this;
    }

    public void Update() {
        updateAccumulator_ += Time.deltaTime;
        if (updateAccumulator_ > updateFrequencyInSeconds_) {
            updateAccumulator_ = 0;

            FPS.Value = 1f / Time.smoothDeltaTime;
            DeltaTime.Value = Time.deltaTime * 1000;
        }
    }

    public void SwitchActiveOption() {
        if (ViewSwitcher.GetChild(0) != ViewSwitcher.ActiveView)
            ViewSwitcher.SwitchTo(0);
        else
            ViewSwitcher.SwitchTo(1);
    }
}
