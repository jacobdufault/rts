using Forge2;

namespace Game.Test {
    public interface IFrame {}

    public class BaseSystem {
        protected void DependsOn<T>() where T : BaseSystem {}
        protected void GetFrame<T>(out T frame) where T : IFrame {
            frame = default(T);
        }
        protected virtual void Initialize() {}
    }

    public class HealthData {
        public class Frame : IFrame {}
    }

    public class SystemA : BaseSystem {
    }

    public class SystemB : BaseSystem {
        private Filter emptyFilter = Filter.Empty();
        private HealthData.Frame healthData;

        protected override void Initialize() {
            DependsOn<SystemA>();
            GetFrame(out healthData);
        }
    }
}