﻿using System;
using System.Collections.Generic;
using Forge2;
using UnityEngine;

namespace Game {
    public struct Position {
        public struct Init {
            public Rect rect;

            public Init(Rect rect) {
                this.rect = rect;
            }
        }

        public class Frame : IFrame, IInitReceiver<Init>, IEntityDestroyed {

            Type IFrame.DataType => typeof(Position);
            Type IInitReceiver.InitType => typeof(Init);

            public SparseEntityDictionary<int> entityToIndex = new SparseEntityDictionary<int>();
            public Bag<HotEntry> hot = new Bag<HotEntry>();
            public Bag<QuadTreeEntry> quadEntries = new Bag<QuadTreeEntry>();

            public SparseEntityDictionary<int> entityToInterpolationIndex = new SparseEntityDictionary<int>();
            public Bag<InterpolationEntry> interpolation = new Bag<InterpolationEntry>();

            public Bag<Entity> updatedPositions = new Bag<Entity>();
            public QuadTree<QuadTreeEntry> spatial;

            private struct PositionUpdate {
                public Entity entity;
                public Rect position;
            }
            private LinearAllocator<PositionUpdate> positionUpdates_ = new LinearAllocator<PositionUpdate>();

            public Frame(Dimension worldSize) {
                // TODO: Expose size to tweaking.
                spatial = QuadTree<QuadTreeEntry>.CreateForDimensions(worldSize, new Dimension(10, 10));
            }

            public void SetPosition(Entity entity, Rect newPosition) {
                positionUpdates_.Allocate() = new PositionUpdate {
                    entity = entity,
                    position = newPosition
                };
            }

            void IFrame.OnFrameCompleted() {
                updatedPositions.Clear();
                foreach (var update in positionUpdates_)
                    updatedPositions.Add(update.entity);

                // Split quad tree update out from current update, as the quad tree
                // update is extremely hard on cache.
                for (int i = 0; i < positionUpdates_.Length; ++i) {
                    ref PositionUpdate update = ref positionUpdates_[i];
                    int idx = entityToIndex[update.entity];

                    // TODO: Remove this if.
                    if (!spatial.bounds.Contains(update.position))
                        update.position.SetPosition(spatial.bounds.centerX, spatial.bounds.centerY);

                    Entity e = quadEntries[idx].entity;
                    spatial.Update(quadEntries[idx], update.position);
                    quadEntries[idx].rect = update.position;
                }

                foreach (var update in positionUpdates_) {
                    int idx = entityToIndex[update.entity];
                    hot[idx].previous = hot[idx].current;
                    hot[idx].current = update.position;
                }

                positionUpdates_.Clear();
            }

            void IInitReceiver<Init>.Init(Entity entity, Init value) {
                hot.Add(new HotEntry {
                    current = value.rect,
                    previous = value.rect
                });
                quadEntries.Add(new QuadTreeEntry {
                    entity = entity,
                    rect = value.rect
                });

                entityToIndex.GetOrInit(entity) = hot.Length - 1;

                spatial.Add(quadEntries[quadEntries.Length - 1]);
            }

            private struct IndexUpdate {
                public Entity moved;
                public int newIndex;
            }

            void IEntityDestroyed.OnDestroyed(List<Entity> destroyed) {
                foreach (Entity entity in destroyed) {
                    int index = entityToIndex[entity];
                    spatial.Remove(quadEntries[index]);
                }

                using (var scratch = new ScratchBuffer<IndexUpdate>(destroyed.Count)) {
                    foreach (Entity entity in destroyed) {
                        int hotQuadIndex = entityToIndex[entity];
                        entityToIndex.Remove(entity);
                        hot.Remove(hotQuadIndex);
                        quadEntries.Remove(hotQuadIndex);

                        // hot[index] could now point to a different entity. We need to
                        // update the reference inside of entityToIndex if that happened.
                        if (hotQuadIndex < hot.Length) {
                            scratch.Add(new IndexUpdate {
                                moved = interpolation[hotQuadIndex].entity,
                                newIndex = hotQuadIndex
                            });
                        }

                        // TODO: Pull logic out here into separate loops.
                        int interpolationIdx = entityToInterpolationIndex[entity];
                        interpolation.Remove(interpolationIdx);
                        if (interpolationIdx < interpolation.Length)
                            entityToInterpolationIndex[interpolation[interpolationIdx].entity] = interpolationIdx;
                    }

                    foreach (IndexUpdate update in scratch)
                        entityToIndex[update.moved] = update.newIndex;
                }
            }
        }

        public struct HotEntry : IEquatable<HotEntry> {
            public Rect current;
            public Rect previous;

            public bool Equals(HotEntry other) {
                return current == other.current && previous == other.previous;
            }
        }
        public struct InterpolationEntry {
            public Entity entity;
            public Transform transform;
            public Vector3 start;
            public Vector3 end;
            public float accumulator;
        }
        public struct QuadTreeEntry : IQuadTreeItem, IEquatable<QuadTreeEntry> {
            public Entity entity;
            public int index { get; set; }
            public Rect rect { get; set; }

            public bool Equals(QuadTreeEntry other) => entity == other.entity;
        }
    }

    public class SyncUnityPosition : BaseSystem {
        [AutoRegister]
        private Filter positions = Filter.AllOf<Position, PrefabData>();
        private Position.Frame positionFrame;
        private PrefabData.Frame prefabFrame;

        protected override void OnInitialized() {
            GetFrame(out positionFrame);
            GetFrame(out prefabFrame);
        }

        public override void Update() {
            var entityToIndex = positionFrame.entityToIndex;
            var entityToInterpolationIndex = positionFrame.entityToInterpolationIndex;
            var hot = positionFrame.hot;
            var interpolation = positionFrame.interpolation;

            // Register new interpolations.
            foreach (Entity entity in positionFrame.updatedPositions) {
                int interpolationIdx;
                if (entityToInterpolationIndex.Has(entity)) {
                    interpolationIdx = entityToInterpolationIndex[entity];
                } else {
                    interpolationIdx = interpolation.Length;
                    entityToInterpolationIndex.Set(entity, interpolationIdx);

                    interpolation.Add(new Position.InterpolationEntry {
                        entity = entity,
                        transform = prefabFrame.storage[entity].instance.transform
                    });
                }

                ref Position.HotEntry hotEntry = ref hot[entityToIndex[entity]];
                ref Position.InterpolationEntry interpolationEntry = ref interpolation[interpolationIdx];
                interpolationEntry.start = hotEntry.previous.ToCenter();
                interpolationEntry.end = hotEntry.current.ToCenter();
                interpolationEntry.accumulator = 0;
            }

            // Run interpolations.
            float deltaTime = Time.deltaTime;
            float duration = App.updateFrequency;
            float durationBeforeRemoving = duration * 4f;
            int i = 0;
            while (i < interpolation.Length) {
                ref Position.InterpolationEntry entry = ref interpolation[i];
                entry.accumulator += deltaTime;
                entry.transform.position = Vector3.Lerp(entry.start, entry.end, entry.accumulator / duration);

                // Wait a little before actually removing the entry so we don't spend extra time removing/readding it
                // for entities which are constantly moving.
                if (entry.accumulator > durationBeforeRemoving) {
                    entityToInterpolationIndex.Remove(entry.entity);
                    // Note: This call will update the entry reference as well.
                    interpolation.Remove(i);
                    if (i < interpolation.Length)
                        entityToInterpolationIndex[interpolation[i].entity] = i;
                } else {
                    ++i;
                }
            }
        }
    }
}
