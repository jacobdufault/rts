using System;
using System.Collections.Generic;
using System.Linq;

namespace Game {
    public class Metric {
        private List<double> values = new List<double>();

        public void AddValue(double value) {
            values.Add(value);
        }

        public override string ToString() => "Average: " + values.Average() + ", StdDev=" + CalculateStdDev();

        private double CalculateStdDev() {
            if (values.Count == 0)
                return 0;

            double avg = values.Average();
            double sum = values.Sum(d => Math.Pow(d - avg, 2));
            return Math.Sqrt((sum) / (values.Count - 1));
        }
    }
}