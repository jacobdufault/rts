﻿using Forge2;
using UnityEngine;

namespace Game {
    public struct WanderData {
        public class Frame : SparseFrame<WanderData> { }

        public float speed;
    }

    public class WanderSystem : BaseSystem {
        [AutoRegister]
        private Filter wanderable = Filter.AllOf<Position, WanderData>();
        private WanderData.Frame wanderFrame;
        private Position.Frame positionFrame;

        protected override void OnInitialized() {
            GetFrame(out wanderFrame);
            GetFrame(out positionFrame);
        }

        public override void Update() {
            foreach (Entity entity in wanderable) {
                if (UnityEngine.Random.Range(0, 4) != 0) continue;

                float speed = wanderFrame[entity].speed;

                Rect currentPosition = positionFrame.hot[positionFrame.entityToIndex[entity]].current;
                Vector2 delta = UnityEngine.Random.insideUnitCircle * speed;
                Rect dest = currentPosition + delta;

                positionFrame.SetPosition(entity, dest);
            }
        }
    }
}
