using System.Collections.Generic;

namespace Game {

    public class TestFoo {
        private Config<int> nodeSize = Config.SyncPerf("Grid.nodeSize", 100);
        private Config<Dimension> worldSize = Config.WorldParam("Grid.worldSize", new Dimension(1000, 1000));
    }

   /**
    * Example usage:
    *
    *   class Test {
    *       private static Config<int> gridSize = new Config<int>("Test.gridSize", ConfigType.WorldParam, 1000);
    *       void Create() {
    *           var grid = new Grid(gridSize.value);
    *       }
    *   }
    */

    // Note: We collect all config values by preinstantiating an empty world with all frames attached. This world is
    //       never actually used.

    public class Config<T> {
        public string name;
        public T value => (T)Config.allValues[name].value;

        public Config(string name, ConfigType type, T value) {
            this.name = name;
            Config.allValues[name] = new Config.Entry {
                type = type,
                value = value
            };
        }
    }

    public enum ConfigType {
        WorldParam,
        SyncedPerformance,
        LocalPerformance,
    }

    public static class Config {
        public struct Entry {
            public ConfigType type;
            public object value;
        }

        private static Dictionary<string, Entry> allValues_;
        public static Dictionary<string, Entry> allValues {
            get {
                if (allValues_ == null)
                    allValues_ = new Dictionary<string, Entry>();
                return allValues_;
            }
        }

        public static Config<T> WorldParam<T>(string name, T value) =>
            new Config<T>(name, ConfigType.WorldParam, value);
        public static Config<T> SyncPerf<T>(string name, T value) =>
            new Config<T>(name, ConfigType.SyncedPerformance, value);
        public static Config<T> LocalPerf<T>(string name, T value) =>
            new Config<T>(name, ConfigType.LocalPerformance, value);
    }
}