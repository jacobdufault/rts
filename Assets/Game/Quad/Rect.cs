﻿using System;
using Mathf = UnityEngine.Mathf;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Game {
    public struct Rect : IEquatable<Rect> {
        public float x0;
        public float y0;
        public float x1;
        public float y1;

        public float left => x0;
        public float top => y0;
        public float right => x1;
        public float bottom => y1;

        public float width => x1 - x0;
        public float height => y1 - y0;

        public float centerX => (x0 + x1) / 2;
        public float centerY => (y0 + y1) / 2;

        public void SetPosition(float x, float y) {
            float w = width;
            float h = height;

            x0 = x;
            y0 = y;
            x1 = x0 + w;
            y1 = y0 + h;
        }

        public static Rect FromWH(float w, float h) {
            return new Rect(0, 0, w, h);
        }
        public static Rect FromXYWH(float x, float y, float w, float h) {
            return new Rect(x, y, x + w, y + h);
        }
        public static Rect FromUnityRect(UnityEngine.Rect rect) {
            return FromXYWH(rect.x, rect.y, rect.width, rect.height);
        }
        public Vector3 ToCenter() {
            return new Vector3((x0 + x1) / 2f, 0, (y0 + y1) / 2f);
        }

        public static Rect operator +(Rect a, Vector2 b) =>
            Rect.FromXYWH(a.x0 + b.x, a.y0 + b.y, a.width, a.height);

        public Rect(float x0, float y0, float x1, float y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }

        // Returns true if this Rect fuly encloses or contains |other|.
        public bool Contains(Rect other) {
            return top <= other.top &&
                   left <= other.left &&
                   bottom >= other.bottom &&
                   right >= other.right;
        }

        public bool Intersects(Rect other) {
            return (other.x1 >= x0 && other.x0 <= x1) && (other.y1 >= y0 && other.y0 <= y1);
        }

        public override string ToString() => $"(x={x0}, y={y0}, w={width}, h={height})";

        public override bool Equals(object other) {
            if (other is Rect)
                return Equals((Rect)other);
            return false;
        }
        public bool Equals(Rect other) {
            return Mathf.Approximately(x0, other.x0) &&
                   Mathf.Approximately(y0, other.y0) &&
                   Mathf.Approximately(x1, other.x1) &&
                   Mathf.Approximately(y1, other.y1);
        }
        public static bool operator ==(Rect a, Rect b) => a.Equals(b);
        public static bool operator !=(Rect a, Rect b) => !a.Equals(b);

        public override int GetHashCode() {
            unchecked {
                int hash = 17;
                hash = hash * 31 + x0.GetHashCode();
                hash = hash * 31 + y0.GetHashCode();
                hash = hash * 31 + x1.GetHashCode();
                hash = hash * 31 + y1.GetHashCode();
                return hash;
            }
        }

    }
}