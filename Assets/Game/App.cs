﻿using System;
using System.Collections.Generic;
using System.Text;
using Forge2;
using Mono.Simd;
using UnityEngine;
using UnityEngine.UI;
using UnityRandom = UnityEngine.Random;

namespace Game {
    public struct Damage : IEquatable<Damage> {
        // TODO: This frame should not support initialization
        public class FrameStorage : UnorderedDenseFrame<Damage> { }

        public int amount;
        public Entity destination;
        public Entity source;

        public bool Equals(Damage other) {
            return amount == other.amount &&
                   destination == other.destination &&
                   source == other.source;
        }
    }

    public class ApplyDamage : BaseSystem {
        private Damage.FrameStorage damageFrame;
        private HealthData.FrameStorage healthFrame;

        protected override void OnInitialized() {
            GetFrame(out damageFrame);
            GetFrame(out healthFrame);
        }

        public override void Update() {
            var wHealthFrame = healthFrame.writable;

            foreach (Damage damage in damageFrame.storage) {
                if (!wHealthFrame.storage.Has(damage.destination))
                    throw new InvalidOperationException("Damage applied to entity with no health");
                wHealthFrame[damage.destination].health -= damage.amount;
            }

            damageFrame.storage.Clear();
        }
    }

    public struct DamageRandomEntity : IEquatable<DamageRandomEntity> {
        public class FrameStorage : UnorderedDenseFrame<DamageRandomEntity, DamageRandomEntity> {
            protected override void Initialize(Entity entity, DamageRandomEntity initData, ref DamageRandomEntity frameData) {
                frameData = initData;
                frameData.owner = entity;
            }
        }

        public Entity owner;
        public int amount;

        public bool Equals(DamageRandomEntity other) {
            return owner == other.owner &&
                   amount == other.amount;
        }
    }

    public class RunDamageRandomEntitySystem : BaseSystem {
        [AutoRegister]
        private Filter candidates = Filter.AllOf<HealthData>();
        private DamageRandomEntity.FrameStorage damageRandom;
        private Damage.FrameStorage postDamage;

        protected override void OnInitialized() {
            GetFrame(out damageRandom);
            GetFrame(out postDamage);
        }

        public override void Update() {
            if (candidates.entities.Length == 0)
                return;

            foreach (DamageRandomEntity data in damageRandom.storage) {
                // Find an entity to target that is *not* the originator.
                int destination = UnityRandom.Range(0, candidates.entities.Length);
                if (destination == data.owner.id)
                    destination = (destination + 1) % candidates.entities.Length;
                if (destination == data.owner.id)
                    continue;

                // Post damage to get processed later.
                postDamage.storage.Add(new Damage {
                    amount = data.amount,
                    source = data.owner,
                    destination = new Entity(destination)
                });
            }
        }

    }


    public struct UniqueId {
        public class FrameStorage : DenseFrame<UniqueId> {
            private int nextUniqueId = 0;

            protected override void Init(Entity entity, UniqueId initData, ref UniqueId frameData) {
                frameData.uniqueId = nextUniqueId++;
            }
        }

        public int uniqueId;

        public override string ToString() {
            return uniqueId.ToString();
        }
    }

    public struct NameData {
        public class FrameStorage : DenseFrame<NameData> { }

        public NameData(string name) {
            this.name = name;
        }

        public string name;

        public override string ToString() {
            return name;
        }
    }

    public struct HealthData {
        public class FrameStorage : SparseVersionedFrame<HealthData> { }

        public HealthData(int health) {
            this.health = health;
        }

        public int health;
    }
    public struct UnitData {
        public class FrameStorage : SparseVersionedFrame<UnitData> { }
    }
    public struct BuildingData {
        public class FrameStorage : SparseVersionedFrame<BuildingData> { }
    }

    public struct RegenData {
        public class FrameStorage : EmptyFrame<RegenData> { }
    }

    public class RegenBuildings : BaseSystem {
        [AutoRegister]
        public Filter toRegen = Filter
            .AllOf<HealthData, RegenData>()
            .AnyOf<UnitData, BuildingData>();
        private HealthData.FrameStorage healthFrame;

        protected override void OnInitialized() {
            GetFrame(out healthFrame);
        }

        public override void Update() {
            foreach (Entity entity in toRegen) {
                ref HealthData health = ref healthFrame.writable[entity];
                health.health += 3;
            }
        }
    }

    public class App : MonoBehaviour {
        public int updatesPerSecond = 15;
        public static float updateFrequency;
        private float accumulator_;

        private Core core;

        private Template monster = Template.Build()
            .Add(new UniqueId())
            .Add(new NameData("Monster"))
            .Add(new PrefabData.Init("Monster"))
            .Add(new WanderData { speed = .3f })
            .Add(new Position.Init(Rect.FromWH(1, 1)))
            .Add(new HealthData(100))
            .Add(new DamageRandomEntity { amount = 1 });

        private Template civilian = Template.Build()
            .Add(new UniqueId())
            .Add(new NameData("Civilian"))
            .Add(new HealthData(10))
            .Add(new UnitData())
            .Add(new RegenData());

        private Template.Overrides john;

        public struct Obj {
            public int a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22;
            public int b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19, b20, b21, b22;
            public int c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20, c21, c22;
        }
        public void AwakeTestPerf() {
            var bag = new Bag<Obj>();
            for (int i = 0; i < 50000; ++i)
                bag.Add(new Obj { a0 = i });

            var sb = new StringBuilder();

            var fast = new Metric();
            var man1 = new Metric();
            var man2 = new Metric();
            var normal = new Metric();

            for (int j = 0; j < 10; ++j) {
                var start = DateTime.Now;
                int sum = 0;
                foreach (Element<Obj> entry in (IEnumerable<Element<Obj>>)bag) {
                    sum += entry.value.a0;
                }
                var elapsed = DateTime.Now - start;
                fast.AddValue(elapsed.TotalMilliseconds);

                // THIS is the fastes
                start = DateTime.Now;
                sum = 0;
                for (int i = 0; i < bag.Length; ++i) {
                    sum += bag.RawItems[i].a0;
                }
                elapsed = DateTime.Now - start;
                man1.AddValue(elapsed.TotalMilliseconds);

                start = DateTime.Now;
                sum = 0;
                for (int i = 0; i < bag.Length; ++i) {
                    sum += bag[i].a0;
                }
                elapsed = DateTime.Now - start;
                man2.AddValue(elapsed.TotalMilliseconds);

                start = DateTime.Now;
                sum = 0;
                foreach (Obj entry in (IEnumerable<Obj>)bag) {
                    sum += entry.a0;
                }
                elapsed = DateTime.Now - start;
                normal.AddValue(elapsed.TotalMilliseconds);
            }

            sb.AppendLine("fast: " + fast);
            sb.AppendLine("man1: " + man1);
            sb.AppendLine("man2: " + man2);
            sb.AppendLine("normal: " + normal);
            Debug.LogError(sb);
        }


        public void Awake() {
            Debug.Log($"AccelMode = {Mono.Simd.SimdRuntime.AccelMode}");
            Vector4i v4f = new Vector4i(0, 0, 0, 0);

            Vector4i.LoadAligned(ref v4f);

            updateFrequency = 1f / updatesPerSecond;

            john = civilian.Override()
                .Add(new NameData("John"));

            var builder = new Core.Builder();

            builder.frames
                .Add<WanderData.Frame>()
                .Add(new PrefabData.Frame(gameObject))
                .Add<Damage.FrameStorage>()
                .Add<DamageRandomEntity.FrameStorage>()
                .Add<UniqueId.FrameStorage>()
                .Add<NameData.FrameStorage>()
                .Add(new Position.Frame(new Dimension(100, 100)))
                .Add<HealthData.FrameStorage>()
                .Add<UnitData.FrameStorage>()
                .Add<BuildingData.FrameStorage>()
                .Add<RegenData.FrameStorage>();

            builder.systems
                .Add<WanderSystem>()
                .Add<SyncUnityPosition>()
                .Add<RunDamageRandomEntitySystem>()
                .Add<ApplyDamage>();
                //.Add<RegenBuildings>();

            core = builder.Build();

            for (int i = 0; i < initialMonster; ++i)
                core.entities.Create(monster);
        }

        public bool alwaysTick;
        public bool tickOnce;
        public bool printOnce;

        public int initialMonster = 0;

        public int monsterCount = 1;
        public int civilianCount = 25;
        public int johnCount = 1;

        public Text text;

        public void Update() {
            if (tickOnce || alwaysTick) {
                accumulator_ += Time.deltaTime;
                if (accumulator_ > updateFrequency) {
                    accumulator_ -= updateFrequency;
                    tickOnce = false;
                    Tick();
                }
            }

            if (printOnce) {
                printOnce = false;
                Print();
            }
        }

        public void Tick() {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            DoTick();
            stopwatch.Stop();
            float ms = stopwatch.ElapsedTicks / (float)System.Diagnostics.Stopwatch.Frequency * 1000f;
            PerformanceView.Instance.SimulationTime.Value = ms;
            PerformanceView.Instance.EntityCount.Value = core.entities.AliveCount;
        }

        private void DoTick() {
            for (int i = 0; i < monsterCount; ++i)
                core.entities.Create(monster);
            for (int i = 0; i < civilianCount; ++i)
                core.entities.Create(civilian);
            for (int i = 0; i < johnCount; ++i)
                core.entities.Create(john);

            core.Update();
        }

        public void Print() {
            foreach (var kvp in core.frames.GetFrame<HealthData.FrameStorage>().readable.storage) {
                Entity entity = kvp.Key;
                HealthData health = kvp.Value;
                NameData name = core.frames.GetFrame<NameData.FrameStorage>()[entity];
                Debug.Log($"{entity} {name} has {health.health} health");
            }
        }
    }
}
